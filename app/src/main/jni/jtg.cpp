//
// Created by aliexdsg on 2/9/2017.
//

#include <jni.h>
//const char k1[10]="-mpzb-";
//const char k2[10]="-tzzq-";
//const char k3[10]="-zkzb-";
//const char k4[10]="-tzzw-";
//const char k5[10]="-zgtz-";
//const char k6[10]="-bank-";
const char k1[20]="ffffffffffff";
const char k2[20]="ffffffffffff";
const char k3[20]="ffffffffffff";
const char k4[20]="ffffffffffff";
const char k5[20]="ffffffffffff";
const char k6[20]="ffffffffffff";
//const char k1[20]="2d6d707a622d";
//const char k2[20]="2d747a7a712d";
//const char k3[20]="2d7a6b7a622d";
//const char k4[20]="2d747a7a772d";
//const char k5[20]="2d7a67747a2d";
//const char k6[20]="2d62616e6b2d";
//2d6d707a622d
//0d0a
//2d747a7a712d
//0d0a
//2d7a6b7a622d
//0d0a
//2d747a7a772d
//0d0a
//2d7a67747a2d
//0d0a
//2d62616e6b2d
const char d1[20]="FFFFFFFFFFFF";
const char d2[20]="FFFFFFFFFFFF";
const char d3[20]="FFFFFFFFFFFF";
const char d4[20]="FFFFFFFFFFFF";
const char d5[20]="FFFFFFFFFFFF";
const char d6[20]="FFFFFFFFFFFF";
const char ip[20]="10.66.10.224";
const char port[20]="8000";
//const char ip[20]="10.66.10.139";
//const char port[20]="80";
const char urlbal[50]="/va/getBalance/";
const char urlinq[50]="/pajak/KOTA/esampah/getInq/";
const char urldep[50]="/va/getDeposit/";
const char urldeprevs[50]="/va/getReversalDeposit/";
const char urlpost[50]="/pajak/KOTA/esampah/getPost";
const char urlpostrevs[50]="/pajak/KOTA/esampah/getRev";
const char urlreg[50]="/controller/getAktivasiStatus/";
const char urllogin[50]="/controller/getLogin/";
const char urlecho[50]="/controller/getEcho/";
//char d1[10]={0x30,0x30,0x30,0x30,0x30,0x30};
//char d2[10]={0x30,0x30,0x30,0x30,0x30,0x30};
//char d3[10]={0x30,0x30,0x30,0x30,0x30,0x30};
//char d4[10]={0x30,0x30,0x30,0x30,0x30,0x30};
//char d5[10]={0x30,0x30,0x30,0x30,0x30,0x30};
//char d6[10]={0x30,0x30,0x30,0x30,0x30,0x30};
//const char d1[10]={0xff,0xff,0xff,0xff,0xff,0xff};
//const char d2[10]={0xff,0xff,0xff,0xff,0xff,0xff};
//const char d3[10]={0xff,0xff,0xff,0xff,0xff,0xff};
//const char d4[10]={0xff,0xff,0xff,0xff,0xff,0xff};
//const char d5[10]={0xff,0xff,0xff,0xff,0xff,0xff};
//const char d6[10]={0xff,0xff,0xff,0xff,0xff,0xff};


extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_loadk(JNIEnv *env, jobject instance, jint i) {
    // TODO
if (i==0){return env->NewStringUTF( k1);}
else if (i==1){return env->NewStringUTF( k2);}
else if (i==2){return env->NewStringUTF( k3);}
else if (i==3){return env->NewStringUTF( k4);}
else if (i==4){return env->NewStringUTF( k5);}
else if (i==5){return env->NewStringUTF( k6);}
else {return env->NewStringUTF( d1);}
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lip(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( ip);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lport(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( port);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lurlbal(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urlbal);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lurlinq(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urlinq);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lurlpost(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urlpost);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lurldep(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urldep);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lurlpostrevs(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urlpostrevs);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lurldeprevs(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urldeprevs);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActReg_lip(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( ip);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActReg_lport(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( port);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActReg_lurlreg(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urlreg);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActLogin_lip(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( ip);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActLogin_lport(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( port);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActLogin_lurllogin(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urllogin);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActReg_lurllogin(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urllogin);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActMain_lurlecho(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urlecho);
}

extern "C" JNIEXPORT jstring JNICALL Java_com_bankjateng_eret_ActLogin_lurlecho(JNIEnv *env, jobject instance) {
    return env->NewStringUTF( urlecho);
}