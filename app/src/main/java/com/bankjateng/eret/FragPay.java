package com.bankjateng.eret;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragPay extends Fragment {

    Button b_pay;

    public FragPay() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ActMain actMain = (ActMain) getActivity();

        View v = inflater.inflate(R.layout.fragment_pay, container, false);

//        Button b_print=(Button) v.findViewById(R.id.bprintpay);
//        b_print.setOnClickListener(actMain.f_printpay);

        TextView tvnpwrd=(TextView) v.findViewById(R.id.tvNPWRD);
        TextView tvkios=(TextView) v.findViewById(R.id.tvKIOS);
        TextView tvsaldo=(TextView) v.findViewById(R.id.tvSALDO);
        tvnpwrd.setVisibility(View.GONE);
        tvkios.setVisibility(View.GONE);
        tvsaldo.setVisibility(View.GONE);
        TextView tvnama=(TextView) v.findViewById(R.id.tvNAMA);
        tvnama.setText("\nSilahkan Tempelkan Kartu\n");

        Button b_scan=(Button) v.findViewById(R.id.bScan);
        b_scan.setText("CEK RETRIBUSI");
        b_scan.setOnClickListener(actMain.f_retpayment);
        b_scan.setVisibility(View.GONE);
        //b_scan.setVisibility(View.GONE);
       // b_scan.setOnClickListener(actMain.f_saldo);

       // b_pay=(Button) v.findViewById(R.id.bpay);

        //b_pay.setOnClickListener(actMain.f_payment);
        //b_pay.setOnClickListener(actMain.f_retpayment);

        return v;

    }

}
