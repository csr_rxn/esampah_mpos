package com.bankjateng.eret;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class trxlistadapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;

    public trxlistadapter(Activity a, final ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)  vi = inflater.inflate(R.layout.itemtrx, null);

        TextView npwrd = (TextView)vi.findViewById(R.id.trxnpwrd); // title
        TextView nama = (TextView)vi.findViewById(R.id.tinfo); // artist name
        TextView amount = (TextView)vi.findViewById(R.id.trxamount); // duration
        TextView reff = (TextView)vi.findViewById(R.id.trxreff); // duration
        TextView date = (TextView)vi.findViewById(R.id.trxdate); // duration
        ImageView imageView = (ImageView) vi.findViewById(R.id.imglist);
        HashMap<String, String> trx = new HashMap<String, String>();
        trx = data.get(position);
        int ints=Integer.parseInt(trx.get(DBHelper.KEY_AMOUNT));

        NumberFormat n;
        n = NumberFormat.getCurrencyInstance();
        String topupformat=n.format(ints);
        topupformat=topupformat.replace("$", "");
        topupformat=topupformat.replace(".00", "");
        topupformat=topupformat.replace(",", ".");

        npwrd.setText(trx.get(DBHelper.KEY_NPWRD));
        nama.setText(trx.get(DBHelper.KEY_NAME));
        amount.setText("Rp. "+topupformat);
        reff.setText(trx.get(DBHelper.KEY_REFF));
        date.setText(trx.get(DBHelper.KEY_DATE));

        //get first letter of each String item
        String firstLetter = String.valueOf(trx.get(DBHelper.KEY_REFF).charAt(0));
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color = generator.getColor(getItem(position));

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(firstLetter, color); // radius in px
        imageView.setImageDrawable(drawable);
        return vi;
    }



}
