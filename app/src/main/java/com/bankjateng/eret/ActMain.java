package com.bankjateng.eret;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.prefs.Preferences;

import justtide.CommandApdu;
import justtide.ContactlessCard;
import justtide.PiccInterface;
import justtide.PiccReader;
import justtide.ResponseApdu;
import justtide.SystemMiscService;
import justtide.ThermalPrinter;

public class ActMain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,PiccInterface {

    SystemMiscService mpos = SystemMiscService.getInstance();
    Preferences prefs = Preferences.userNodeForPackage(ActLogin.class);
    private CoordinatorLayout coordinatorLayout;
    private static final int ZBAR_SCANNER_REQUEST = 0;
    private static final int ZBAR_QR_SCANNER_REQUEST = 1;

    int state=0;

    static final char[] hexArray = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    public String CMD="";
    Button bTambah,bKurang;
    static Tag Kartu;

    public TextView cardno;
    public TextView cardbal;

    public EditText etopup;
    public EditText ebayar;
    String buff2Print="";

    NavigationView navigationView = null;
    Toolbar toolbar = null;

    //  byte[] buffer = new byte[1000];


    private TextView UserLogin;
    private TextView IDLogin;
    private boolean mIsBluetoothConnected = false;
    Button mConnectBtn;
    Button mEnableBtn;
    Button mBScan;
    Spinner mDeviceSp;
    ProgressDialog mProgressDlg;
    ProgressDialog mConnectingDlg;

//    ArrayList<BluetoothDevice> mDeviceList = new ArrayList<BluetoothDevice>();
//    public BluetoothAdapter mBluetoothAdapter	= BluetoothAdapter.getDefaultAdapter();

    // Session Manager Class
    SessionManager session;
    public static Activity ma;
    SQLiteDatabase db;

    Settings settings;
    MyCount timerLogout;
    private serviceReceiver receiver=new serviceReceiver();
    //
    protected static final int REFRESH_TICKET_TEXT = 0;
    protected static final int REFRESH_STATE = 1;
    protected static final int ERROR = 2;

    private byte[] buffer;

    ThermalPrinter thermalPrinter = ThermalPrinter.getInstance();


    protected static final String TAG = "BJTG";
    protected static final int CHECK_SUCCESS = 0;
    protected static final int RESPONSE_APDU = 1;
    protected static final int OPEN_FAIL = 2;
    protected static final int REFRESH = 3;
    protected static final int TIME_OUT = 4;
    protected static final int USER_CANCEL = 5;
    protected static final int REFRESH_EXC = 6;
    protected static final int REFRESH_M = 7;

    private TextView showStateText;
    private TextView showDataText;
    private TextView showApduText;
    private Handler mHandler = null;
    private Thread piccThread;
    private String stateString = "";
    private String dataShowString = "";
    private String strResponseApdu = "";
    private String strPiccException = "";
    private ContactlessCard contactlessCard ;
    private ResponseApdu responseApdu ;
    private CommandApdu commandApdu;
    private static byte[] apdu = {0x00, (byte)0xA4, 0x00, 0x00, 0x02, 0x3f, 0x00,(byte)0xff};
    private static byte[] apduuid = {(byte)0xFF, (byte)0xCA, (byte)0x00, (byte)0x00, (byte)0x00 };

    PiccReader piccReader;
    String CityCode="";
    String buff="";
    String NAMA="";
    String NPWRD="";
    String SALDO="";
    String SN="";
    String KIOS="";
    String TOPUP="";
    TelephonyManager  tm;
    String IMEI="";
    String USIM="";
    FragmentManager fm=null;

    TextView tv_NPWRD;
    TextView tv_NAMA;
    TextView tv_SALDO;
    TextView tv_SN;
    TextView tv_KIOS;
    RadioButton rb_10;
    RadioButton rb_25;
    RadioButton rb_50;
    RadioButton rb_100;
    RadioButton rb_150;
    int cardstate=0;
    String pagestate="";
    int Readerstate=0;
    int counttime;
    static{
        System.loadLibrary("jtg");
    }
    //    public native  byte[] datab1();
//    public native  byte[] datab2();
//    public native  byte[] datab3();
//    public native  byte[] datab4();
//    public native  byte[] datab5();
//    public native  byte[] datab6();
    public native String loadk(int i);
    public native String lip();
    public native String lport();
    public native String lurlbal();
    public native String lurlinq();
    public native String lurlpost();
    public native String lurldep();
    public native String lurlpostrevs();
    public native String lurldeprevs();
    public native String lurlecho();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ma=this;
        mProgressDlg = new ProgressDialog(this);
        mConnectingDlg = new ProgressDialog(this);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        //Set the fragment initially
        FragMain mainfragment = new FragMain();
        android.support.v4.app.FragmentTransaction fragmentTransaction =
                getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, mainfragment);
        fragmentTransaction.commit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        session = new SessionManager(getApplicationContext());
        // Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();
        session.checkLogin();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        String name = user.get(SessionManager.KEY_NAME);
        String menus = user.get(SessionManager.KEY_EMAIL);
        // displaying user data
        //How to change elements in the header programatically
        View headerView = navigationView.getHeaderView(0);

        //TextView emailText = (TextView) headerView.findViewById(R.id.email);
        UserLogin = (TextView) headerView.findViewById(R.id.tvUserLogin);
        IDLogin = (TextView) headerView.findViewById(R.id.tvIDLogin);
        //emailText.setText("newemail@email.com");
        // UserLogin.setText(Html.fromHtml("Name: <b>" + name + "</b>"));
        // IDLogin.setText(Html.fromHtml("Email: <b>" + email + "</b>"));
        UserLogin.setText(name);
        IDLogin.setText("");
        IDLogin.setVisibility(View.GONE);
        NavigationView navView=(NavigationView) findViewById(R.id.nav_view);

        Menu navMenu =  navView.getMenu();
        navView.setItemTextColor(ColorStateList.valueOf(Color.WHITE));
//        SubMenu fmenu =  navMenu.addSubMenu("Menu baru");
//        fmenu.add("1");
//        fmenu.add("2");
//        fmenu.add("3");
//        001001 pembayaran eret
//        002001 topup
//        002002 cek saldo
//        003001 ganti password
        navMenu.findItem(R.id.pay_ment).setVisible(false);
        navMenu.findItem(R.id.top_up).setVisible(false);
        navMenu.findItem(R.id.cek_saldo).setVisible(false);
        // navMenu.findItem(R.id.repassword).setVisible(false);
        String[] res=menus.split(",");
        for(int i=0;i<res.length;i++){
            if (res[i].equals("001001")) navMenu.findItem(R.id.pay_ment).setVisible(true);
            if (res[i].equals("002001")) navMenu.findItem(R.id.top_up).setVisible(true);
            if (res[i].equals("002002")) navMenu.findItem(R.id.cek_saldo).setVisible(true);
            //if (res[i].equals("003001")) navMenu.findItem(R.id.repassword).setVisible(true);
        }
        timerLogout = new MyCount(20*60*1000, 1);
        db = openOrCreateDatabase(DBHelper.DATABASE_NAME,android.content.Context.MODE_PRIVATE ,null);
//        ActivityHelper.initialize(this);
        //  conblue();

        fm=getSupportFragmentManager();

        Calendar c= Calendar.getInstance();
        Date date = c.getTime();
        c.setTime ( date ); // convert your date to Calendar object
        int daysToDecrement = -30;
        c.add(Calendar.DATE, daysToDecrement);
        // again get back your date object
        SimpleDateFormat iddata = new SimpleDateFormat("yyyyMMddHHmmss");
        System.out.println(iddata.format(c.getTime()));
        DBHelper dbHelper=new DBHelper(getApplicationContext());
        dbHelper.delOldtrx(dbHelper.KEY_ID,iddata.format(c.getTime()));
//        DBHelper dbHelper=new DBHelper(getApplicationContext());
//        int x;
//        for (x=1002000;x<1010000;x++) {
//            dbHelper.intrx(Integer.toString(x), "DB"+Integer.toString(x), "tes"+Integer.toString(x), "100000", "10000", "NPWRD"+Integer.toString(x),"06/04/2017 15:55:51", "toko");
//        }
        pagestate="home";
        counttime=0;
    }
    public void initprint(){
        buffer = new byte[48];
        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == REFRESH_TICKET_TEXT) {
                    //ticketText.setText(ticketString);
                    // showStateText.setText(stateString);
                } else if (msg.what == ERROR) {
                    // showStateText.setText(stateString);
                }
                super.handleMessage(msg);
            }
        };
    }
    public void initpicc(){
        print("Reader opened");
        final Handler handler = new Handler();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Readerstate=1;
                picc = null;
                picc = new Thread( new Runnable() {
                    @Override
                    public void run() {
                        while (Readerstate==1) {
                            try {
                                if (pagestate == "topup" || pagestate == "payment" || pagestate == "balance") {
                                    piccReader = null;
                                    contactlessCard = null;
                                    piccReader = PiccReader.getInstance();
                                    piccReader.close();
                                    piccReader.open();
                                    piccReader.search(ContactlessCard.TYPE_UNKOWN, 10000, ActMain.this);
                                    if (cardstate == 1 && (pagestate == "topup" || pagestate == "payment" || pagestate == "balance")) {
                                        try {
                                            playsound();
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    tv_NAMA = (TextView) findViewById(R.id.tvNAMA);
                                                    if (tv_NAMA != null)
                                                        tv_NAMA.setText("\nMembaca Kartu ...\n");
                                                }
                                            });
                                            NPWRD = "";
                                            NAMA = "";
                                            SALDO = "";
                                            SN = "";
                                            KIOS = "";
                                            //   print("Open PICC....");
                                            //  Log.i("PICCDemo","++++begin search++++");
                                            if (contactlessCard.getType() == ContactlessCard.TYPE_MIFARE) {

                                                char Block;
                                                String[] data = new String[5];
                                                stateString = "";
                                                int sector = 4;
                                                byte[] pw = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
                                                //byte[] pw = {(byte) 0x65,(byte) 0x70,(byte) 0x6c,(byte) 0x69,(byte) 0x6e,(byte) 0x6b};
                                                byte[] serialNo = contactlessCard.getSerialNo();
                                                byte[] BlkValue = new byte[20];
                                                byte[] test = new byte[20];
                                                int ret = -1;
                                                test[0] = (byte) serialNo.length;
                                                System.arraycopy(serialNo, 0, test, 1, serialNo.length);
                                                print(fc_BytesToHex(serialNo));
                                                for (int n = 0; n < 5; n++) {
                                                    {
                                                        pw = hexStringToByteArray(loadk(n));
                                                        String buffread = "";
                                                        Block = (char) ((sector + n) * 4);
                                                        //  Log.v(TAG, "m1Authority begin ");
                                                        ret = piccReader.m1Authority('b', Block, pw, test);
                                                        //  Log.v(TAG, "m1Authority ret = "+ret);
                                                        if (ret == 0) {
                                                            //stateString += "Authority successful \n";
                                                            for (int inc = 0; inc < 3; inc++) {
                                                                Block = (char) (((sector + n) * 4) + inc);
                                                                ret = piccReader.m1ReadBlock(Block, BlkValue);
                                                                if (ret == 0) {
                                                                    stateString += "Block value:" + byteToString(BlkValue, 16);
                                                                    buffread += byteToString(BlkValue, 16);
                                                                } else {
                                                                    print("Read Block " + (((sector + n) * 4) + inc) + "Fail " + ret);
                                                                    //stateString += "Read block fail";
                                                                }
                                                            }
                                                            data[n] = buffread;
                                                        } else {
                                                            print("Auth Block " + ((sector + n) * 4) + "Fail " + ret);
                                                            showtoast("Kartu tidak valid");
                                                            break;
                                                            //stateString += "Authority fail \n";
                                                        }

                                                    }
                                                    // print(data[n]);
                                                }
//                                NPWRD="R200000000309";
//                                NAMA="BAYU SAPUTRO";
//                                SN="082345678901";
                                                if (data[0].length() > 1)
                                                    buff=convertHexToString(data[0]).replaceAll("[^A-Za-z0-9 ]", "");
                                                Log.e("++++++ CSR", data[0]);
                                                CityCode=buff.substring(0,4);
                                                NPWRD = buff.substring(4);
                                                if (data[1].length() > 1)
                                                    NAMA = convertHexToString(data[1]).replaceAll("[^A-Za-z0-9.,' ]", "");
                                                //if (data[2].length()>1) SALDO=convertHexToString(data[2]).replaceAll("[^0-9]", "");;
                                                if (data[3].length() > 1)
                                                    SN = convertHexToString(data[3]).replaceAll("[^A-Za-z0-9 ]", "");
                                                //if (data[4].length()>1) KIOS=convertHexToString(data[4]).replaceAll("[^A-Za-z0-9 ]", "");;

                                                print(NPWRD + " " + NAMA + " " + SALDO + " " + SN + " " + KIOS);
                                                showtoast(NPWRD + "\n" + NAMA);
                                                if (NPWRD.length() < 2 | NAMA.length() < 2 | SN.length() < 2) {
                                                    msgbox("Silahkan Ulangi");

                                                } else {
                                                    Thread n = new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (pagestate.equals("balance")) {
                                                                readcard();
                                                            } else if (pagestate.equals("payment")) {
                                                                readcard();

                                                                eretinq();
                                                            } else if (pagestate.equals("topup")) {
                                                                readcard();
                                                            }
                                                        }
                                                    });
                                                    n.start();
                                                }
                                            }

                                        } catch (Exception e) {
                                            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                                            e.printStackTrace();
                                        }
                                        piccReader.waitForRemove(contactlessCard);
                                        piccReader.close();
                                        if ((pagestate == "topup" || pagestate == "payment" || pagestate == "balance")) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    // wait=null;
                                                    tv_NPWRD = (TextView) findViewById(R.id.tvNPWRD);
                                                    tv_NAMA = (TextView) findViewById(R.id.tvNAMA);
                                                    tv_SALDO = (TextView) findViewById(R.id.tvSALDO);

                                                    if (tv_NPWRD != null)
                                                        tv_NPWRD.setVisibility(View.GONE);
                                                    if (tv_NAMA != null)
                                                        tv_NAMA.setVisibility(View.VISIBLE);
                                                    if (tv_SALDO != null)
                                                        tv_SALDO.setVisibility(View.GONE);

                                                    if (tv_NPWRD != null)
                                                        tv_NPWRD.setText("NPWRD ");
                                                    if (tv_NAMA != null)
                                                        tv_NAMA.setText("\nSilahkan Tempelkan Kartu\n");
                                                    if (tv_SALDO != null)
                                                        tv_SALDO.setText("SALDO ");
                                                }
                                            });
                                            //                                if (contactlessCard.getType() == ContactlessCard.TYPE_MIFARE) {
//                                    String cmd;
//                                    char Block;
//                                    String[] data = new String[5];
//                                    stateString = "";
//                                    int sector = 4;
//                                    byte[] pw = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
//                                    //byte[] pw = {(byte) 0x65,(byte) 0x70,(byte) 0x6c,(byte) 0x69,(byte) 0x6e,(byte) 0x6b};
//                                    byte[] serialNo = contactlessCard.getSerialNo();
//                                    byte[] BlkValue = new byte[20];
//                                    byte[] test = new byte[20];
//                                    int ret = -1;
//                                    test[0] = (byte) serialNo.length;
//                                    System.arraycopy(serialNo, 0, test, 1, serialNo.length);
//
//                                    for (int n = 0; n < 5; n++) {
//                                        {
//                                            String buffread = "";
//                                            Block = (char) ((sector + n) * 4);
//                                            //  Log.v(TAG, "m1Authority begin ");
//                                            ret = piccReader.m1Authority('a', Block, pw, test);
//                                            //  Log.v(TAG, "m1Authority ret = "+ret);
//                                            if (ret == 0) {
//                                                stateString += "Authority successful \n";
//                                            } else {
//                                                print("Auth Block "+((sector + n) * 4)+ "Fail "+ret);
//                                                stateString += "Authority fail \n";
//                                            }
//                                            for (int inc = 0; inc < 3; inc++) {
//                                                Block = (char) (((sector + n) * 4) + inc);
//                                                ret = piccReader.m1ReadBlock(Block, BlkValue);
//                                                //       Log.v(TAG, "m1ReadBlock ret = "+ret);
//                                                if (ret == 0) {
//                                                    // byte[] blockValue = new byte[BlkValue[0]];
//                                                    //   System.arraycopy(BlkValue , 1, blockValue, 0, BlkValue[0]);
//
//                                                    stateString += "Block value:" + byteToString(BlkValue, 16);
//                                                    buffread += byteToString(BlkValue, 16);
//                                                } else {
//                                                    print("Read Block "+(((sector + n) * 4) + inc)+ "Fail "+ret);
//                                                    stateString += "Read block fail";
//                                                }
//                                            }
//                                            data[n] = buffread;
//                                        }
//                                        print(convertHexToString(data[n]).replaceAll("[^A-Za-z0-9 ]", ""));
//                                    }
//
//                                }
                                        }

//                                NPWRD="";    NAMA="";   SALDO="";     SN="";   KIOS="";
                                    }
                                } else {
                                    Thread.sleep(1000);
                                }
                            } catch (Exception e) {
                                print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                                e.printStackTrace();
                            }
                        }
                    }
                });
                if (picc.isAlive()==false) picc.start();
            }
        }, 1000);

        mHandler = new Handler() {
            public void handleMessage(Message msg) {
                if(msg.what == CHECK_SUCCESS) {
                }else if(msg.what == OPEN_FAIL)	{
                    piccThread = null;
                    //    print(stateString);
                } else if (msg.what == REFRESH) {
                    //    print(stateString);
                } else if (msg.what == USER_CANCEL) {
                    //     print("picc demo cancel");
                    piccThread = null;
                } else if (msg.what == TIME_OUT) {
                    //     print("picc timeout");
                    piccThread = null;
                } else if (msg.what == REFRESH_EXC){
                    //      print(stateString);
                } else if(msg.what == REFRESH_M){
                    //    print(stateString);
                }
                super.handleMessage(msg);
            }
        };
    };
    Thread picc;
    public void playsound(){
        (new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                } catch (Exception e) {
                    print("Play Sound Error "+e.toString());
                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                    e.printStackTrace();
                }


            }
        })).start();
    }
//    Runnable picccard = new Runnable() {
//        @Override
//        public void run() {
//            while (Readerstate==1) {
//                try {
//                    piccReader = null;
//                    contactlessCard = null;
//                    piccReader = PiccReader.getInstance();
//                    piccReader.close();
//                    piccReader.open();
//                    piccReader.search(ContactlessCard.TYPE_UNKOWN, 10000, ActMain.this);
//                    if (cardstate == 1 && (pagestate=="topup"|| pagestate=="payment"||pagestate=="balance")) {
//                        try {
//                            playsound();
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    tv_NAMA=(TextView) findViewById(R.id.tvNAMA);
//                                    if (tv_NAMA!=null) tv_NAMA.setText("\nMembaca Kartu ...\n");
//                                }
//                            });
//                            NPWRD="";    NAMA="";   SALDO="";     SN="";   KIOS="";
//                            //   print("Open PICC....");
//                            //  Log.i("PICCDemo","++++begin search++++");
//                            if(contactlessCard.getType() == ContactlessCard.TYPE_MIFARE) {
//
//                                char Block;
//                                String[] data = new String[5];
//                                stateString = "";
//                                int sector = 4;
//                                byte[] pw = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
//                                //byte[] pw = {(byte) 0x65,(byte) 0x70,(byte) 0x6c,(byte) 0x69,(byte) 0x6e,(byte) 0x6b};
//                                byte[] serialNo = contactlessCard.getSerialNo();
//                                byte[] BlkValue = new byte[20];
//                                byte[] test = new byte[20];
//                                int ret = -1;
//                                test[0] = (byte) serialNo.length;
//                                System.arraycopy(serialNo, 0, test, 1, serialNo.length);
//                                for (int n = 0; n < 5; n++) {
//                                    {
//                                        pw = hexStringToByteArray(loadk(n));
//                                        String buffread = "";
//                                        Block = (char) ((sector + n) * 4);
//                                        //  Log.v(TAG, "m1Authority begin ");
//                                        ret = piccReader.m1Authority('b', Block, pw, test);
//                                        //  Log.v(TAG, "m1Authority ret = "+ret);
//                                        if (ret == 0) {
//                                            //stateString += "Authority successful \n";
//                                            for (int inc = 0; inc < 3; inc++) {
//                                                Block = (char) (((sector + n) * 4) + inc);
//                                                ret = piccReader.m1ReadBlock(Block, BlkValue);
//                                                if (ret == 0) {
//                                                    stateString += "Block value:" + byteToString(BlkValue, 16);
//                                                    buffread += byteToString(BlkValue, 16);
//                                                } else {
//                                                    print("Read Block " + (((sector + n) * 4) + inc) + "Fail " + ret);
//                                                    //stateString += "Read block fail";
//                                                }
//                                            }
//                                            data[n] = buffread;
//                                        } else {
//                                            print("Auth Block " + ((sector + n) * 4) + "Fail " + ret);
//                                            showtoast("Kartu tidak valid");
//                                            break;
//                                            //stateString += "Authority fail \n";
//                                        }
//
//                                    }
//                                    //   print(data[n]);
//                                }
////                                NPWRD="R200000000309";
////                                NAMA="BAYU SAPUTRO";
////                                SN="082345678901";
//                                if (data[0].length() > 1) NPWRD = convertHexToString(data[0]).replaceAll("[^A-Za-z0-9 ]", "");
//                                if (data[1].length() > 1) NAMA = convertHexToString(data[1]).replaceAll("[^A-Za-z0-9.,' ]", "");
//                                //if (data[2].length()>1) SALDO=convertHexToString(data[2]).replaceAll("[^0-9]", "");;
//                                if (data[3].length() > 1) SN = convertHexToString(data[3]).replaceAll("[^A-Za-z0-9 ]", "");
//                                //if (data[4].length()>1) KIOS=convertHexToString(data[4]).replaceAll("[^A-Za-z0-9 ]", "");;
//
//                                print(NPWRD + " " + NAMA + " " + SALDO + " " + SN + " " + KIOS);
//                                showtoast(NPWRD + "\n" + NAMA);
//                                if (NPWRD.length() < 2 | NAMA.length() < 2 | SN.length() < 2) {
//                                    msgbox("Silahkan Ulangi");
//
//                                } else {
//                                    Thread n = new Thread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            if (pagestate.equals("balance")) {
//                                                readcard();
//                                            } else if (pagestate.equals("payment")) {
//                                                readcard();
//                                                eretinq();
//                                            } else if(pagestate.equals("topup")){
//                                                readcard();
//                                            }
//                                        }
//                                    });
//                                    n.start();
//                                }
//                            }
//
//                        } catch (Exception e) {
//                            print(e.toString());
//                            e.printStackTrace();
//                        }
//                        //                                if (contactlessCard.getType() == ContactlessCard.TYPE_MIFARE) {
////                                    String cmd;
////                                    char Block;
////                                    String[] data = new String[5];
////                                    stateString = "";
////                                    int sector = 4;
////                                    byte[] pw = {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF};
////                                    //byte[] pw = {(byte) 0x65,(byte) 0x70,(byte) 0x6c,(byte) 0x69,(byte) 0x6e,(byte) 0x6b};
////                                    byte[] serialNo = contactlessCard.getSerialNo();
////                                    byte[] BlkValue = new byte[20];
////                                    byte[] test = new byte[20];
////                                    int ret = -1;
////                                    test[0] = (byte) serialNo.length;
////                                    System.arraycopy(serialNo, 0, test, 1, serialNo.length);
////
////                                    for (int n = 0; n < 5; n++) {
////                                        {
////                                            String buffread = "";
////                                            Block = (char) ((sector + n) * 4);
////                                            //  Log.v(TAG, "m1Authority begin ");
////                                            ret = piccReader.m1Authority('a', Block, pw, test);
////                                            //  Log.v(TAG, "m1Authority ret = "+ret);
////                                            if (ret == 0) {
////                                                stateString += "Authority successful \n";
////                                            } else {
////                                                print("Auth Block "+((sector + n) * 4)+ "Fail "+ret);
////                                                stateString += "Authority fail \n";
////                                            }
////                                            for (int inc = 0; inc < 3; inc++) {
////                                                Block = (char) (((sector + n) * 4) + inc);
////                                                ret = piccReader.m1ReadBlock(Block, BlkValue);
////                                                //       Log.v(TAG, "m1ReadBlock ret = "+ret);
////                                                if (ret == 0) {
////                                                    // byte[] blockValue = new byte[BlkValue[0]];
////                                                    //   System.arraycopy(BlkValue , 1, blockValue, 0, BlkValue[0]);
////
////                                                    stateString += "Block value:" + byteToString(BlkValue, 16);
////                                                    buffread += byteToString(BlkValue, 16);
////                                                } else {
////                                                    print("Read Block "+(((sector + n) * 4) + inc)+ "Fail "+ret);
////                                                    stateString += "Read block fail";
////                                                }
////                                            }
////                                            data[n] = buffread;
////                                        }
////                                        print(convertHexToString(data[n]).replaceAll("[^A-Za-z0-9 ]", ""));
////                                    }
////
////                                }
//                    }
//
//                    piccReader.waitForRemove(contactlessCard);
//                    piccReader.close();
//                    if ((pagestate=="topup"|| pagestate=="payment"||pagestate=="balance")){
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                // wait=null;
//                                tv_NPWRD=(TextView) findViewById(R.id.tvNPWRD);
//                                tv_NAMA=(TextView) findViewById(R.id.tvNAMA);
//                                tv_SALDO=(TextView) findViewById(R.id.tvSALDO);
//
//                                if (tv_NPWRD!=null) tv_NPWRD.setVisibility(View.GONE);
//                                if (tv_NAMA!=null) tv_NAMA.setVisibility(View.VISIBLE);
//                                if (tv_SALDO!=null) tv_SALDO.setVisibility(View.GONE);
//
//                                if (tv_NPWRD!=null)tv_NPWRD.setText("NPWRD ");
//                                if (tv_NAMA!=null)tv_NAMA.setText("\nSilahkan Tempelkan Kartu\n");
//                                if (tv_SALDO!=null)tv_SALDO.setText("SALDO ");
//                            }
//                        });
////                                NPWRD="";    NAMA="";   SALDO="";     SN="";   KIOS="";
//                    }
//                } catch (Exception e) {
//                    print(e.toString());
//                    e.printStackTrace();
//                    //    e.printStackTrace();
//                }
//            }
//        }
//    };

    private String byteToString(byte[] arg, int length){
        String str="", strTemp="";
        int temp;
        for(int i=0;i<length;i++) {
            temp = (int)arg[i] & 0xff;
            if(temp <= 0xf){
                strTemp = "0";
                strTemp += Integer.toHexString(arg[i] & 0xff);
            }else {
                strTemp = Integer.toHexString(arg[i] & 0xff);
            }
            str = str+strTemp;
        }
        return str;
    }
    private String IntToString(int arg){
        String str="", strTemp="";
        int temp;

        temp = (int)arg &0xff;
        if(temp <= 0xf){
            strTemp = "0";
            strTemp += Integer.toHexString(arg & 0xff);
        }else {
            strTemp = Integer.toHexString(arg & 0xff);
        }
        str = str+strTemp;

        return str;
    }
    public String convertHexToString(String hex){

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for( int i=0; i<hex.length()-1; i+=2 ){

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char)decimal);

            temp.append(decimal);
        }
        //   print("Decimal : " + temp.toString());

        return sb.toString();
    }
    public String convformat(String nominal){
        int ints = Integer.parseInt(nominal);
        NumberFormat n;
        n = NumberFormat.getCurrencyInstance();
        String retr = n.format(ints);
        retr = retr.replace("$", "");
        retr = retr.replace(".00", "");
        retr = retr.replace(",", ".");
        return retr;
    }
    protected String rand(int len) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < len) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
    public void readcard(){

        proses = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    tv_NPWRD=(TextView) findViewById(R.id.tvNPWRD);
                    tv_NAMA=(TextView) findViewById(R.id.tvNAMA);
                    tv_SALDO=(TextView) findViewById(R.id.tvSALDO);
                    tv_SN=(TextView) findViewById(R.id.tvSN);
                    tv_KIOS = (TextView) findViewById(R.id.tvKIOS);

                    settings =new Settings(getApplicationContext());
                    HashMap<String,String> dev=settings.getSetting();
                    String user = dev.get(Settings.USERNAME);

                    startDialog();
                    final String Saldoserver = GetBalance(NPWRD, user);
                    handler.sendEmptyMessage(0);

                    print("Saldo " + Saldoserver);
                    if (Saldoserver.equals("-0") == false) {
                        SALDO = Saldoserver;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //  wait=null;
                                int isaldo = Integer.parseInt(Saldoserver);
                                NumberFormat n;
                                n = NumberFormat.getCurrencyInstance();
                                String saldoformat = n.format(isaldo);
                                saldoformat = saldoformat.replace("$", "");
                                saldoformat = saldoformat.replace(".00", "");
                                saldoformat = saldoformat.replace(",", ".");

                                tv_NPWRD.setVisibility(View.VISIBLE);
                                tv_NAMA.setVisibility(View.VISIBLE);
                                tv_SALDO.setVisibility(View.VISIBLE);
                                tv_NPWRD.setText("NPWRD " + NPWRD);
                                tv_NAMA.setText(NAMA);
                                tv_SALDO.setText("Rp. " + saldoformat);
                                tv_SN.setText("SN " + SN);
                                tv_KIOS.setText("KIOS " + KIOS);

                                return;
                            }
                        });
                    } else if (Saldoserver.equals("-0")) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                // wait=null;
                                tv_NPWRD.setVisibility(View.GONE);
                                tv_NAMA.setVisibility(View.VISIBLE);
                                tv_SALDO.setVisibility(View.GONE);
                                tv_NPWRD.setText("NPWRD ");
                                tv_NAMA.setText("\nSilahkan Tempelkan Kartu\n");
                                tv_SALDO.setText("SALDO ");

                                return;
                            }
                        });
                    }
                } catch (Exception e) {
                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                    e.printStackTrace();
                    handler.sendEmptyMessage(0);
                } finally {
                    handler.sendEmptyMessage(0);
                }

            }
        });
        if (proses.isAlive()==false){

            proses.start();
            wait_dialogclose(proses);
        }

    }
    Thread proses;
    public void eretinq(){
        // if (thermalPrinter.isPaperOut() == false) {
        proses = new Thread(new Runnable() {
            public void run() {
                String resp="-1";
                try {
                    if (NPWRD.length() < 2 | NAMA.length() < 2 | SN.length() < 2) {// | SALDO.length() < 2
                        msgbox("Silahkan ulangi tap kartu, tunggu hingga selesai");
                        //handler.sendEmptyMessage(0);
                        return;
                    }
                    int i=0;
                    for (i=0;i<10;i++) handler.sendEmptyMessage(0);

                    Thread.sleep(1000);

                    startDialog();
                    resp = GetInquiryRet(NPWRD);
                    handler.sendEmptyMessage(0);

                } catch (Exception e) {
                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                    e.printStackTrace();
                    handler.sendEmptyMessage(0);
                } finally {
                    handler.sendEmptyMessage(0);
                    if (resp.equals("-0") == false && resp.equals("-1") == false) {
                        BufferPayment = resp;
                        FragInq fragment = new FragInq();
                        FragmentTransaction fragmentTransaction =
                                fm.beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, fragment);
                        fragmentTransaction.addToBackStack("tag").commit();
                        pagestate = "payment1";


                    } else {
                        pagestate = "payment";
                        FragPay fragment = new FragPay();
                        FragmentTransaction fragmentTransaction =
                                fm.beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, fragment);
                        fragmentTransaction.commit();
                        NPWRD = "";
                        NAMA = "";
                        SALDO = "";
                        SN = "";
                        KIOS = "";


                        // handler.sendEmptyMessage(0);

                    }

                }

            }

        });
        if (proses.isAlive()==false){
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    startDialog();
//                }
//            });
            proses.start();
            wait_dialogclose(proses);
        }

        //     } else {
        //handler.sendEmptyMessage(0);
        //        msgbox("Isi Ulang Kertas dahulu\nsebelum melakukan Pembayaran");
        //    }
    }
    public void EretPayData(View v){
//        int i=0;
//        for (i=0;i<10;i++) {try{  pd.dismiss(); Thread.sleep(100);} catch (Exception e) { ; }}
        TextView tvjudul = (TextView) v.findViewById(R.id.tvJudul);
        final String[] res = BufferPayment.split("#");
        Log.e("++++ CSR PARSING", BufferPayment);
        JSONObject jsonObject = null;
        final StringBuffer sb = new StringBuffer();
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        try {
            jsonObject = new JSONObject(res[1]);
            Log.e("++++ JSONOBJECT", jsonObject.toString());
            sb.append("ID BILLING         : " + jsonObject.getString("id_billing") + "\n");
            sb.append("Nama                 : " + jsonObject.getString("nama") + "\n");
            sb.append("Kecamatan        : " + jsonObject.getString("kecamatan") + "\n");
            sb.append("Kelurahan          : " + jsonObject.getString("kelurahan") + "\n");
            sb.append("RW/RT               : " + jsonObject.getString("rw")+ "/" + jsonObject.getString("rt") + "\n");
            sb.append("Nominal Pokok : " + formatRupiah.format(Double.valueOf(jsonObject.getString("nominal_pokok"))) + "\n");
            sb.append("Nominal Denda : " + formatRupiah.format(Double.valueOf(jsonObject.getString("nominal_denda"))) + "\n");
            sb.append("Nominal Total   : " + formatRupiah.format(Double.valueOf(jsonObject.getString("nominal_total"))));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int ints=Integer.parseInt(res[0]);
        if(res.length<2) { try{ pd.dismiss(); } catch (Exception e) { e.printStackTrace(); }msgbox("Silahkan Ulangi");fm.popBackStack();return;};
        NumberFormat n;
        n = NumberFormat.getCurrencyInstance();
        String retr=n.format(ints);
        retr=retr.replace("$", "");
        retr=retr.replace(".00", "");
        retr=retr.replace(",", ".");
        tvjudul.setText("PAJAK RETRIBUSI");

        ArrayList<String> rjs = new ArrayList<String>();
        rjs.add(NAMA);
        rjs.add("NPWRD "+NPWRD);
        //rjs.add(res[1]);
        rjs.add(sb.toString());
        rjs.add("RETRIBUSI Rp. "+retr);
        Button bOK=(Button) v.findViewById(R.id.bItemOK);
        Button bCA=(Button) v.findViewById(R.id.bItemCancel);
        bOK.setText("BAYAR");
        bCA.setText("BATAL");
        if (rjs.size()>0){
            ListView lvdata = (ListView) v.findViewById(R.id.lvItem);
            lvdata.setVisibility(View.VISIBLE);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, R.id.lstContent,rjs);
            lvdata.setAdapter(adapter);
        }
        bOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proses = new Thread(new Runnable() {
                    public void run() {
                        try {
                            print("Klik Bayar");

                            int jmlcetak = Integer.parseInt(PreferenceHelper.getSecureSharedPreference(getApplication(), getApplicationContext())
                                    .getString("jmlcetak", "2"));
                            if (jmlcetak > 0 && thermalPrinter.isPaperOut() == true) {
                                msgbox("Isi Ulang Kertas dahulu\nsebelum melakukan Pembayaran");

                                return;
                            }
                            //                            if (resp.equals(-0) == false) {

                            print(res[0]);
                            int payment = Integer.parseInt(res[0]);
                            String saldoakhir = "";

                            if (Integer.parseInt(SALDO) < payment) {
                                msgbox("Saldo Anda Tidak Cukup\n"+NAMA+"\n"+NPWRD+"\nSaldo Rp. "+SALDO);
                                //handler.sendEmptyMessage(0);

                                return;
                            } else {
                                String user = "";
                                String code = "";
                                String ws = "";
                                settings = new Settings(getApplicationContext());
                                HashMap<String, String> dev = settings.getSetting();
                                user = dev.get(Settings.USERNAME);
                                code = dev.get(Settings.CODE);
                                ws = dev.get(Settings.WS);

                                Calendar c = Calendar.getInstance();
                                SimpleDateFormat dtm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//                                SimpleDateFormat tm = new SimpleDateFormat("MMdd");

                                String tgljam = dtm.format(c.getTime());

                                //String refd = tm.format(c.getTime());
                                // String ref = "B" + code + refd + Integer.toString(random);

                                SimpleDateFormat bln = new SimpleDateFormat("MM");
                                SimpleDateFormat tgl = new SimpleDateFormat("dd");
                                //SimpleDateFormat jmd = new SimpleDateFormat("HHmmss");
                                String refd = String.format("B%s%02X%01X%02X%s",
                                        code,
                                        Integer.parseInt(user.substring(8)),
                                        Integer.parseInt(bln.format(c.getTime())),
                                        Integer.parseInt(tgl.format(c.getTime())),
                                        rand(5));
                                if (user != null) {

                                } else {
                                    msgbox("User Tidak valid");

                                    return;
                                }
                                //    print(saldo);

                                String PAYMENT = Integer.toString(payment);

                                if (NPWRD.length() == 0 && NAMA.length() == 0 && SALDO.length() == 0 && SN.length() == 0) {

                                    msgbox("Silahkan Ulangi");
                                    fm.popBackStack();

                                    return;
                                }
                                handler.sendEmptyMessage(0);

                                startDialog();                                  ;
                                // saldoakhir = GetPostRet(user, NPWRD,NAMA, ws,SN,PAYMENT, ref);
                                saldoakhir = PostPostRet(user, NPWRD,NAMA, ws,SN,PAYMENT, refd);
                                handler.sendEmptyMessage(0);
                                print(saldoakhir);
                                if (saldoakhir.equals("-0") == false && saldoakhir.equals("-1") == false) {
                                    playsound();
                                    int saldo = Integer.parseInt(saldoakhir);
                                    String strsaldo = Integer.toString(saldo);
                                    SALDO = strsaldo;
                                    SimpleDateFormat iddata = new SimpleDateFormat("yyyyMMddHHmmss");
                                    DBHelper dbHelper = new DBHelper(getApplicationContext());
                                    dbHelper.intrx(iddata.format(c.getTime()), refd, NAMA, PAYMENT, SALDO, NPWRD, tgljam, res[1].replace("   ", ""));
                                    printtrx( IMEI,tgljam+" : "+refd+"\t"+NAMA+"\t"+PAYMENT+"\t"+SALDO+"\t"+NPWRD+"\t"+tgljam+"\t"+res[1].replace("   ", ""));
                                    if (jmlcetak > 0) {if( printpayment(res[1].replace("   ", ""), NAMA, PAYMENT, refd, tgljam)) {
                                        ;
                                    } else {
                                        msgbox("Cetak Gagal");
                                    }}else {
                                        /*String message="";
                                        message=message+("BLOK : " + res[1].replace("   ", "") + "\n");
                                        message=message+("PEMILIK  : " + NAMA + "\n");
                                        message=message+("RETRIBUSI: Rp. " + convformat(PAYMENT) + "\n");
                                        message=message+("NO REFF  : " + refd + "\n");
                                        message=message+("TGL/JAM  : " + tgljam + "\n");
                                        msgbox(message+"\nTransaksi Berhasil");};*/

                                        String message="";
                                        message=message+("NOMOR VA      : " + res[1].replace("   ", "") + "\n");
                                        message=message+("NAMA          : " + NAMA + "\n");
                                        message=message+("KECAMATAN     : Rp. " + convformat(PAYMENT) + "\n");
                                        message=message+("KELURAHAN     : " + refd + "\n");
                                        message=message+("RW/RT         : " + tgljam + "\n");
                                        message=message+("NOMINAL POKOK : " + tgljam + "\n");
                                        message=message+("NOMINAL DENDA : " + tgljam + "\n");
                                        message=message+("TOTAL         : " + tgljam + "\n");
                                        msgbox(message+"\nTransaksi Berhasil");};
                                    if ((jmlcetak - 1) > 0) {
                                        int n = 0;
                                        for (n = jmlcetak - 1; n > 0; n--)
                                            printpayment2(res[1].replace("   ", ""), NAMA, PAYMENT, refd, tgljam);
                                    }
                                    //handler.sendEmptyMessage(0);
                                    //msgbox("Pembayaran berhasilb");
                                    if (pagestate == "payment1") {
                                        pagestate = "payment";
                                    }
                                    NPWRD = "";
                                    NAMA = "";
                                    SALDO = "";
                                    SN = "";
                                    KIOS = "";
                                    fm.popBackStack();

                                    return;
                                } else if (saldoakhir.equals("-1") == true) {
                                    startDialog();

                                    saldoakhir = PostRevsPostRet(user, NPWRD, NAMA, ws, SN, PAYMENT, refd + "r");
                                    if (saldoakhir.equals("-1") == true) {
                                        saldoakhir = PostRevsPostRet(user, NPWRD, NAMA, ws, SN, PAYMENT, refd + "r");
                                        if (saldoakhir.equals("-1") == true) {
                                            saldoakhir = PostRevsPostRet(user, NPWRD, NAMA, ws, SN, PAYMENT, refd + "r");
                                        }
                                    }
                                    handler.sendEmptyMessage(0);
                                    if (pagestate == "payment1") {
                                        pagestate = "payment";
                                    }
                                    fm.popBackStack();
                                    msgbox("Pembayaran Tidak berhasil");
                                    NPWRD = "";
                                    NAMA = "";
                                    SALDO = "";
                                    SN = "";
                                    KIOS = "";

                                    return;
                                }

                            }
                        } catch (Exception e) {
                            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                            e.printStackTrace();
                            handler.sendEmptyMessage(0);
                        } finally {
                            handler.sendEmptyMessage(0);
                        }
                    }
                });
                if (proses.isAlive()==false) {

                    proses.start();
                    wait_dialogclose(proses);
                }

            }
        });
        bCA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                print("Klik Batal");
                if (pagestate=="payment1"){
                    pagestate="payment";
                }
                fm.popBackStack();

                return;
            }
        });
    }
    public void topup(){
        proses = new Thread(new Runnable() {
            public void run() {
                try {
                    print("Klik Topup");
                    int jmlcetak = Integer.parseInt(PreferenceHelper.getSecureSharedPreference(getApplication(), getApplicationContext())
                            .getString("jmlcetak", "2"));
                    if (jmlcetak > 0 && thermalPrinter.isPaperOut() == true) {
                        msgbox("Isi Ulang Kertas dahulu\nsebelum melakukan Top Up");
                        return;
                    }

                    if (NPWRD.length() < 2 | NAMA.length() < 2 | SN.length() < 2) {
                        msgbox("Silahkan ulangi tap kartu, tunggu hingga selesai");

                        //handler.sendEmptyMessage(0);
                        return;
                    }
                    int mounttopup = 0;
                    rb_10 = (RadioButton) findViewById(R.id.rb10);
                    rb_25 = (RadioButton) findViewById(R.id.rb25);
                    rb_50 = (RadioButton) findViewById(R.id.rb50);
                    rb_100 = (RadioButton) findViewById(R.id.rb100);
                    rb_150 = (RadioButton) findViewById(R.id.rb150);
//                    if (SALDO.length() < 3) {
//                        msgbox("Silakan Cek Saldo terlebih dulu");
//                        return;
//                    }

                    if (rb_10.isChecked() == true || rb_25.isChecked() == true || rb_50.isChecked() == true || rb_100.isChecked() == true || rb_150.isChecked() == true) {
                        if (rb_10.isChecked() == true) {
                            mounttopup = 10000;
                        } else if (rb_25.isChecked() == true) {
                            mounttopup = 25000;
                        } else if (rb_50.isChecked() == true) {
                            mounttopup = 50000;
                        } else if (rb_100.isChecked() == true) {
                            mounttopup = 100000;
                        } else if (rb_150.isChecked() == true) {
                            mounttopup = 150000;
                        }
                    } else {
                        //handler.sendEmptyMessage(0);
                        msgbox("Silahkan Pilih Nominal Deposit terlebih dulu");
                        NPWRD = "";
                        NAMA = "";
                        SALDO = "";
                        SN = "";
                        KIOS = "";

                        return;
                    }
                    String user = "";
                    String code = "";
                    String ws = "";
                    settings = new Settings(getApplicationContext());
                    HashMap<String, String> dev = settings.getSetting();
                    user = dev.get(Settings.USERNAME);
                    code = dev.get(Settings.CODE);
                    ws = dev.get(Settings.WS);
                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat dtm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                    String tgljam = dtm.format(c.getTime());
                    //String refd = tm.format(c.getTime());
                    //  String ref = "D" + code + refd + Integer.toString(random);
                    SimpleDateFormat bln = new SimpleDateFormat("MM");
                    SimpleDateFormat tgl = new SimpleDateFormat("dd");
                    String refd = String.format("D%s%02X%01X%02X%s",
                            code,
                            Integer.parseInt(user.substring(8)),
                            Integer.parseInt(bln.format(c.getTime())),
                            Integer.parseInt(tgl.format(c.getTime())),
                            rand(5));

                    startDialog();
                    String resp = GetTopupRet(user, NPWRD, NAMA, refd, ws, SN, Integer.toString(mounttopup));
                    handler.sendEmptyMessage(0);

                    if (resp.equals("-0") == false && resp.equals("-1") == false) {
                        playsound();
                        SimpleDateFormat iddata = new SimpleDateFormat("yyyyMMddHHmmss");
                        DBHelper dbHelper = new DBHelper(getApplicationContext());
                        dbHelper.intrx(iddata.format(c.getTime()), refd, NAMA, Integer.toString(mounttopup), resp, NPWRD, tgljam, "-");
                        printtrx( IMEI,tgljam+" : "+refd+"\t"+NAMA+"\t"+Integer.toString(mounttopup)+"\t"+resp+"\t"+NPWRD+"\t"+tgljam+"\t"+"-");
                        if (jmlcetak > 0) {
                            if (printtopup(NPWRD, NAMA, Integer.toString(mounttopup), refd, tgljam, resp)) {     ;
                            } else {
                                msgbox("Cetak Gagal");
                            }
                        } else {
                            String message="";
                            message=message+("NPWRD   : " +NPWRD+ "\n");
                            message=message+("NAMA    : " +NAMA+ "\n");
                            message=message+("TOP UP  : Rp. " + convformat(String.valueOf(mounttopup)) + "\n");
                            message=message+("NO REFF : " + refd + "\n");
                            message=message+("TGL/JAM : " + tgljam + "\n");
                            message=message+("SALDO   : Rp. " + convformat(resp) + "\n");
                            msgbox(message+"\nTransaksi Berhasil");};

                        if ((jmlcetak - 1) > 0) {
                            int n = 0;
                            for (n = jmlcetak - 1; n > 0; n--)
                                printtopup2(NPWRD, NAMA, Integer.toString(mounttopup), refd, tgljam, resp);
                        }
                        //msgbox("Deposit Dana Berhasil");
                        NPWRD = "";
                        NAMA = "";
                        SALDO = "";
                        SN = "";
                        KIOS = "";

                        return;

                    } else if (resp.equals("-1") == true) {

                        startDialog();

                        resp = GetRevsTopupRet(user, NPWRD, NAMA, refd + "r", ws, SN, Integer.toString(mounttopup));
                        if (resp.equals("-1") == true) {
                            resp = GetRevsTopupRet(user, NPWRD, NAMA, refd + "r", ws, SN, Integer.toString(mounttopup));
                            if (resp.equals("-1") == true) {
                                resp = GetRevsTopupRet(user, NPWRD, NAMA, refd + "r", ws, SN, Integer.toString(mounttopup));
                            }
                        }

                        handler.sendEmptyMessage(0);
                        msgbox("Deposit Dana Tidak Berhasil");
                        NPWRD = "";
                        NAMA = " ";
                        SALDO = "";
                        SN = "";
                        KIOS = "";

                        return;
                    }
                } catch (Exception e) {
                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                    e.printStackTrace();
                    handler.sendEmptyMessage(0);
                } finally {
                    handler.sendEmptyMessage(0);
                }
            }
        });
        if (proses.isAlive()==false) {

            proses.start();
            wait_dialogclose(proses);
        }
    }
    public void changepass(){
        try {
            settings = new Settings(ActMain.this);
            HashMap<String, String> dev = settings.getSetting();
            String user = dev.get(Settings.USERNAME);
            String pass = dev.get(Settings.PASSWORD);
            String ws = dev.get(Settings.WS);
            String code = dev.get(Settings.CODE);
            EditText txtpass0 = (EditText) findViewById(R.id.txtPass0);
            EditText txtpass1 = (EditText) findViewById(R.id.txtPass1);
            EditText txtpass2 = (EditText) findViewById(R.id.txtPass2);

            if (txtpass0.getText().toString().equals(pass) == false) {
                msgbox("Password Lama tidak sama!");
                return;
            }
            if (txtpass1.getText().toString().length() < 5 | txtpass2.getText().toString().length() < 5) {
                msgbox("Password kurang dari 5 karakter");
                return;
            }
            if (txtpass1.getText().toString().equals(txtpass2.getText().toString()) == false) {
                msgbox("Password Baru yang dimasukkan tidak sama!");
                return;
            }
            String newpass = txtpass1.getText().toString();
            settings.savedatauser(user, newpass,ws,code);

            Thread.sleep(100);
            dev = settings.getSetting();
            pass = dev.get(Settings.PASSWORD);
            if (newpass.equals(pass)) {
                msgbox("Ganti Password Sukses");
                txtpass0.setText("");
                txtpass1.setText("");
                txtpass2.setText("");
            } else {
                msgbox("Ganti Password Gagal");
                txtpass0.setText("");
                txtpass1.setText("");
                txtpass2.setText("");
            }
        } catch(Exception e){
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();};

    }
    Button.OnClickListener f_retpayment = new Button.OnClickListener(){
        public void onClick(View v) {  BufferPayment="";  eretinq();
        }
    };
    public String GetBalance(String va, String user){
        //http://192.168.66.109:81/smartcard/getBalance/{"trxcod":"004905","merchant":"4565","uid":"12345","reader":"987654321","hp":"085640301325","va_acc":"301160112360023"}
        String Url="http://"+lip()+":"+lport()+lurlbal();
        String nominal="";
        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();
            sjhttp.put("va", va);
            sjhttp.put("user", user);
            sjhttp.put("IMEI", IMEI);
            sjhttp.put("USIM", USIM);
            print(Url+sjhttp.toString());
            String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
            print(httpresp.toString());
            // Toast.makeText(this,httpresp.toString(),Toast.LENGTH_LONG).show();
            //print(Url+sjhttp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");

                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                // print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";
                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();
                jrespv = rjhttp.get("saldo_akhir");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) nominal=jrespv.toString();

                if (respcode.equals("00")){
                    return nominal;
                }
                else {
                    msgbox(respcode+":"+desccode);
                    return "-0";}
            } else  {
                if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                msgbox("Tidak dapat terhubung ke Server");
                return"-1";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            msgbox("Data Tidak ketemu atau tidak terhubung ke server");
            return"-1";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public String GetTopupRet(String user, String va, String nama,String no_reff,String ws,String sn,String nominal){

        //http://192.168.66.109:81/smartcard/getBalance/{"trxcod":"004905","merchant":"4565","uid":"12345","reader":"987654321","hp":"085640301325","va_acc":"301160112360023"}
        String Url="http://"+lip()+":"+lport()+lurldep();

        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();
            sjhttp.put("user", user);
            sjhttp.put("va", va);
            sjhttp.put("nama", nama);
            sjhttp.put("no_reff", no_reff);
            sjhttp.put("ws", ws);
            sjhttp.put("sn_card", sn);
            sjhttp.put("amount", Integer.parseInt(nominal));
            sjhttp.put("IMEI", IMEI);
            sjhttp.put("USIM", USIM);

            print(Url+sjhttp.toString());
            String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
            print(httpresp.toString());
            // Toast.makeText(this,httpresp.toString(),Toast.LENGTH_LONG).show();
            //print(Url+sjhttp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");

                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                //print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";
                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();
                jrespv = rjhttp.get("saldo_akhir");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) nominal=jrespv.toString();

                if (respcode.equals("00")){
                    return nominal;

                }
                else {msgbox(respcode+":"+desccode);
                    return "-0";}
            } else  {
                if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                msgbox("Tidak dapat terhubung ke Server");
                return "-1";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            msgbox("Data Tidak ketemu atau tidak terhubung ke server");
            return "-1";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public String GetRevsTopupRet(String user, String va, String nama,String no_reff,String ws,String sn,String nominal){

        String Url="http://"+lip()+":"+lport()+lurldeprevs();
        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();
            sjhttp.put("user", user);
            sjhttp.put("va", va);
            sjhttp.put("nama", nama);
            sjhttp.put("no_reff", no_reff);
            sjhttp.put("ws", ws);
            sjhttp.put("sn_card", sn);
            sjhttp.put("amount", Integer.parseInt(nominal));
            sjhttp.put("IMEI", IMEI);
            sjhttp.put("USIM", USIM);
            print(Url+sjhttp.toString());
            String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
            print(httpresp.toString());
            // Toast.makeText(this,httpresp.toString(),Toast.LENGTH_LONG).show();
            //print(Url+sjhttp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");
                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                //print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";
                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();
                jrespv = rjhttp.get("saldo_akhir");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) nominal=jrespv.toString();
                if (respcode.equals("00")){
                    return nominal;
                }
                else {msgbox(respcode+":"+desccode);
                    return "-0";}
            } else  {
                if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                msgbox("Tidak dapat terhubung ke Server");
                return "-1";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            msgbox("Data Tidak ketemu atau tidak terhubung ke server");
            return "-1";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public String PostPostRet(String user,String va, String nama,String ws,String sn, String tagihan, String no_reff){
        settings = new Settings(ActMain.this);
        HashMap<String, String> dev = settings.getSetting();
        String code = dev.get(Settings.CODE);
        //http://192.168.66.109:81/smartcard/getBalance/{"trxcod":"004905","merchant":"4565","uid":"12345","reader":"987654321","hp":"085640301325","va_acc":"301160112360023"}
        String Url="http://"+lip()+":"+lport()+lurlpost().replace("KOTA",code);
        String nominal="";
        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();
            sjhttp.put("user", user);
            sjhttp.put("va", va);
            sjhttp.put("nama", nama);
            sjhttp.put("ws", ws);
            sjhttp.put("sn_card", sn);
            sjhttp.put("amount", Integer.parseInt(tagihan));
            sjhttp.put("no_reff", no_reff);
            print(Url+sjhttp.toString());
            String httpresp=sendPost(Url,sjhttp);
            print(httpresp.toString());
            // Toast.makeText(this,httpresp.toString(),Toast.LENGTH_LONG).show();
            //print(Url+sjhttp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");

                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                //print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";
                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();
                jrespv = rjhttp.get("saldo_akhir");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) nominal=jrespv.toString();

                if (respcode.equals("00")){
                    return nominal;

                }
                else {msgbox(respcode+":"+desccode);
                    return "-0";}
            } else  {
                if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                msgbox("Tidak dapat terhubung ke Server");
                return "-1";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            msgbox("Data Tidak ketemu atau tidak terhubung ke server");  return "-1";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public String GetPostRet(String user,String va, String nama,String ws,String sn, String tagihan, String no_reff){
        //http://192.168.66.109:81/smartcard/getBalance/{"trxcod":"004905","merchant":"4565","uid":"12345","reader":"987654321","hp":"085640301325","va_acc":"301160112360023"}
        String Url="http://"+lip()+":"+lport()+lurlpost();
        String nominal="";
        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();
            sjhttp.put("user", user);
            sjhttp.put("va", va);
            sjhttp.put("nama", nama);
            sjhttp.put("ws", ws);
            sjhttp.put("sn_card", sn);
            sjhttp.put("amount", Integer.parseInt(tagihan));
            sjhttp.put("no_reff", no_reff);
            sjhttp.put("IMEI", IMEI);
            sjhttp.put("USIM", USIM);
            print(Url+sjhttp.toString());
            String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
            print(httpresp.toString());
            // Toast.makeText(this,httpresp.toString(),Toast.LENGTH_LONG).show();
            //print(Url+sjhttp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");

                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                //print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";
                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();
                jrespv = rjhttp.get("saldo_akhir");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) nominal=jrespv.toString();

                if (respcode.equals("00")){
                    return nominal;

                }
                else {msgbox(respcode+":"+desccode);
                    return "-0";}
            } else  {
                if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                msgbox("Tidak dapat terhubung ke Server");
                return "-0";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            msgbox("Data Tidak ketemu atau tidak terhubung ke server");  return "-0";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public String PostRevsPostRet(String user,String va, String nama,String ws,String sn, String tagihan, String no_reff){
        settings = new Settings(ActMain.this);
        HashMap<String, String> dev = settings.getSetting();
        String code = dev.get(Settings.CODE);
        String Url="http://"+lip()+":"+lport()+lurlpostrevs().replace("KOTA",code);
        String nominal="";
        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();
            sjhttp.put("user", user);
            sjhttp.put("va", va);
            sjhttp.put("nama", nama);
            sjhttp.put("ws", ws);
            sjhttp.put("sn_card", sn);
            sjhttp.put("amount", Integer.parseInt(tagihan));
            sjhttp.put("no_reff", no_reff);
            print(Url+sjhttp.toString());
            String httpresp=sendPost(Url,sjhttp);
            print(httpresp.toString());
            // Toast.makeText(this,httpresp.toString(),Toast.LENGTH_LONG).show();
            //print(Url+sjhttp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");
                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                //print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";
                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();
                jrespv = rjhttp.get("saldo_akhir");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) nominal=jrespv.toString();
                if (respcode.equals("00")){
                    return nominal;

                }
                else {
                    if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                    msgbox(respcode+":"+desccode);
                    return "-0";}
            } else  {

                msgbox("Tidak dapat terhubung ke Server");
                return "-0";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            msgbox("Data Tidak ketemu atau tidak terhubung ke server");   return "-0";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public String GetRevsPostRet(String user,String va, String nama,String ws,String sn, String tagihan, String no_reff){

        String Url="http://"+lip()+":"+lport()+lurlpostrevs();
        String nominal="";
        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();
            sjhttp.put("user", user);
            sjhttp.put("va", va);
            sjhttp.put("nama", nama);
            sjhttp.put("ws", ws);
            sjhttp.put("sn_card", sn);
            sjhttp.put("amount", Integer.parseInt(tagihan));
            sjhttp.put("no_reff", no_reff);
            sjhttp.put("IMEI", IMEI);
            sjhttp.put("USIM", USIM);
            print(Url+sjhttp.toString());
            String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
            print(httpresp.toString());
            // Toast.makeText(this,httpresp.toString(),Toast.LENGTH_LONG).show();
            //print(Url+sjhttp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");
                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                //print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";
                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();
                jrespv = rjhttp.get("saldo_akhir");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) nominal=jrespv.toString();
                if (respcode.equals("00")){
                    return nominal;

                }
                else {
                    if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                    msgbox(respcode+":"+desccode);
                    return "-0";}
            } else  {

                msgbox("Tidak dapat terhubung ke Server");
                return "-0";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            msgbox("Data Tidak ketemu atau tidak terhubung ke server");   return "-0";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public String GetInquiryRet(String id_npwrd){
        settings = new Settings(ActMain.this);
        HashMap<String, String> dev = settings.getSetting();
        String code = dev.get(Settings.CODE);
        String nominal="-0";
        //http://192.168.66.109:81/smartcard/getBalance/{"trxcod":"004905","merchant":"4565","uid":"12345","reader":"987654321","hp":"085640301325","va_acc":"301160112360023"}
        String Url="http://"+lip()+":"+lport()+lurlinq().replace("KOTA",code);

        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();
            sjhttp.put("va", id_npwrd);
            sjhttp.put("IMEI", IMEI);
            sjhttp.put("USIM", USIM);
            print(Url+sjhttp.toString());
            String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
            print(httpresp.toString());
            //  Toast.makeText(this,httpresp.toString(),Toast.LENGTH_LONG).show();
            //print(Url+sjhttp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");
                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                //print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";

                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();
                jrespv = rjhttp.get("tagihan");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) nominal=jrespv.toString();

                if (respcode.equals("00")){
                    String data="";
                    jrespv = rjhttp.get("data");
                    if (jrespv != null &&
                            jrespv.toString().contains("null")==false &&
                            jrespv.toString().length()>0)
                    {data=jrespv.toString();}
                    else data="-";

                    return nominal+"#"+data+"#";

                }
                else {
                    msgbox(respcode+":"+desccode);
                    return "-0";}
            } else  {
                if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                msgbox("Tidak dapat terhubung ke Server");
                return"-1";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            msgbox("Data Tidak ketemu atau tidak terhubung ke server");  return"0";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public String GetEcho( String user){
        //http://192.168.66.109:81/smartcard/getBalance/{"trxcod":"004905","merchant":"4565","uid":"12345","reader":"987654321","hp":"085640301325","va_acc":"301160112360023"}
        String Url="http://"+lip()+":"+lport()+lurlecho();
        String nominal="";
        try {
            ArrayList<String> rjs = new ArrayList<String>();
            JSONObject sjhttp = new JSONObject();

            sjhttp.put("username", user);
            sjhttp.put("IMEI", IMEI);
            sjhttp.put("USIM", USIM);
            print(Url+sjhttp.toString());
            String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
            print(httpresp.toString());
            if (httpresp.toString().contains("200#")){
                String[] res=httpresp.split("#");

                JSONObject rjhttp=new JSONObject(res[1]);
                int lrjhttp=rjhttp.length();
                // print(res[1]);
                Object jrespv;
                String respcode="";
                String desccode="";
                jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
                jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();

                if (respcode.equals("00")){
                    if (desccode.equals("Sukses")==false) {
                        DBHelper dbHelper=new DBHelper(getApplicationContext());
                        if (desccode.equals(dbHelper.getlastmsg())==false){
                            Calendar c = Calendar.getInstance();
                            SimpleDateFormat iddata = new SimpleDateFormat("yyyyMMddHHmmss");
                            SimpleDateFormat dtm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            dbHelper.inmsg(iddata.format(c.getTime()),dtm.format(c.getTime()),desccode);
                            addNotification(getApplicationContext().getResources().getString(R.string.app_name), desccode);
                        }
                    }
                    return "";
                }
                else {
                    showtoast(respcode+":"+desccode);
                    return "-0";}
            } else  {
                if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
                //msgbox("Tidak dapat terhubung ke Server");
                return"-1";
            }

        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
            if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
            // msgbox("Data Tidak ketemu atau tidak terhubung ke server");
            return"-0";
        }
        // cardbal.setText("Saldo :");
        //cardno.setText("No.     :");
    }
    public void addNotification(final String title,final String msg) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.icobjtg)
                        .setContentTitle(title)
                        .setContentText(msg);

        Intent notificationIntent = new Intent(this, ActMain.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }
    Button.OnClickListener f_listhistory  = new Button.OnClickListener(){
        public void onClick(View v) {
            gettrxhistory(v);        }
    };

    private ProgressDialog pd;
    void print(String msg){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dtm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String tgljam = dtm.format(c.getTime());
        System.out.println(tgljam+" : "+msg);
        printlog(IMEI,tgljam+" : "+msg);
    }
    public void printtrx(String filename,String msg) {
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat dtm = new SimpleDateFormat("yyyyMM");
            SimpleDateFormat d = new SimpleDateFormat("yyyyMMdd");
            String thnbln = dtm.format(c.getTime());
            String day = d.format(c.getTime());
            StringBuilder info = new StringBuilder();
            info.append(msg);

            File root = android.os.Environment.getExternalStorageDirectory();
//            String currentDateTimeString = DateFormat.getDateTimeInstance().format(
//                    new Date());
            File dir = new File(root.getAbsolutePath() + "/TRXERET/"+thnbln);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(dir, filename+day+".txt");
            BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
            buf.append(info.toString());
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void printlog(String filename,String msg) {
        try {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat dtm = new SimpleDateFormat("yyyyMM");
            SimpleDateFormat d = new SimpleDateFormat("yyyyMMdd");
            String thnbln = dtm.format(c.getTime());
            String day = d.format(c.getTime());
            StringBuilder info = new StringBuilder();
            info.append(msg);

            File root = android.os.Environment.getExternalStorageDirectory();
//            String currentDateTimeString = DateFormat.getDateTimeInstance().format(
//                    new Date());
            File dir = new File(root.getAbsolutePath() + "/ERET/"+thnbln);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            File file = new File(dir, filename+day+".txt");
            BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
            buf.append(info.toString());
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void dellog(int days) {
        try {
            //String day = d.format(c.getTime());
            //StringBuilder info = new StringBuilder();
            //info.append(msg);
            Calendar c= Calendar.getInstance();
            Date date = c.getTime();
            c.setTime ( date ); // convert your date to Calendar object
            int daysToDecrement = -days;
            c.add(Calendar.DATE, daysToDecrement);
            // again get back your date object
            SimpleDateFormat dtm =new SimpleDateFormat("yyyyMM");
            String thnbln = dtm.format(c.getTime());
            print(thnbln);

            File root = android.os.Environment.getExternalStorageDirectory();
//            String currentDateTimeString = DateFormat.getDateTimeInstance().format(
//                    new Date());
            File dir = new File(root.getAbsolutePath() + "/ERET/"+thnbln);
            if (dir.exists()) {
                if (dir.isDirectory())
                {
                    String[] children = dir.list();
                    for (int i = 0; i < children.length; i++)
                    {
                        new File(dir, children[i]).delete();
                    }
                }
                dir.delete();
                print(dir.toString()+" delete ok");
            } else {print(dir.toString()+" not found");}

//            File file = new File(dir, day+".txt");
//            BufferedWriter buf = new BufferedWriter(new FileWriter(file, true));
//            buf.append(info.toString());
//            buf.newLine();
//            buf.close();
        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
        }
    }
    private void startDialog()  {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pd = ProgressDialog.show(ActMain.this, null, "Mohon Tunggu ... ");
            }
        });
    }
    public void wait_dialogclose(final Thread n){
        (new Thread(new Runnable() {
            @Override
            public void run() {
                while(n.isAlive()) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                handler.sendEmptyMessage(0);
                //  piccThread = null;
            }
        })).start();
    }
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(pd!=null){ pd.dismiss(); }
            try{ pd.dismiss(); } catch (Exception e) { ; }
        }
    };
    public boolean printtest(){

        try {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.

            }
            String IMEI = tm.getDeviceId();
            String USIM = tm.getSimSerialNumber();
            settings = new Settings(getApplicationContext());
            HashMap<String, String> dev = settings.getSetting();
            String user = dev.get(Settings.USERNAME);
            Calendar c = Calendar.getInstance();
            SimpleDateFormat dtm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String tgljam = dtm.format(c.getTime());


            thermalPrinter.initBuffer();
            thermalPrinter.setGray(2);
            Resources res = getResources();
            Bitmap bitmap1 = BitmapFactory.decodeResource(res, R.mipmap.logo);
            thermalPrinter.printLogo(0, 0, bitmap1);

            thermalPrinter.setHeightAndLeft(0, 0);
            thermalPrinter.setLineSpacing(5);
            thermalPrinter.setDispMode(ThermalPrinter.UMODE);
            //thermalPrinter.setFont(ThermalPrinter.ASC12X24,ThermalPrinter.HZK12);
            thermalPrinter.setFont(ThermalPrinter.ASC12X24, ThermalPrinter.HZK12);
            thermalPrinter.setStep(12);
            thermalPrinter.print("TEST CETAK e-PAY\n");

            thermalPrinter.print("USER : " + user + "\n");
            thermalPrinter.print("IMEI : " + IMEI + "\n");

            thermalPrinter.print("SIM  : " + USIM + "\n");
            thermalPrinter.print("TGL/JAM : " + tgljam + "\n");

            thermalPrinter.setStep(200);
            thermalPrinter.printStart();
            thermalPrinter.waitForPrintFinish();

            print(" TEST CETAK e-PAY\n");
            print(" USER : " + user + "\n");
            print(" IMEI : " + IMEI + "\n");

            print(" SIM  : " + USIM + "\n");
            print(" TGL/JAM : " + tgljam + "\n");
        } catch (Exception e){
            e.printStackTrace();
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            return false;
        } finally {
            return  true;
        }
    }
    public boolean printtopup(String idNPWRD,String nama,String topup, String noreff,String tgljam,String saldoakhir){
        try {
            settings = new Settings(ActMain.this);
            HashMap<String, String> dev = settings.getSetting();
            String code = dev.get(Settings.CODE);
            thermalPrinter.initBuffer();
            thermalPrinter.setGray(2);
            Resources res = getResources();
            Bitmap bitmap1 = BitmapFactory.decodeResource(res, R.mipmap.logo);
            thermalPrinter.printLogo(0, 0, bitmap1);

            thermalPrinter.setHeightAndLeft(0, 0);
            thermalPrinter.setLineSpacing(5);
            thermalPrinter.setDispMode(ThermalPrinter.UMODE);
            //thermalPrinter.setFont(ThermalPrinter.ASC12X24,ThermalPrinter.HZK12);
            thermalPrinter.setFont(ThermalPrinter.ASC12X24, ThermalPrinter.HZK12);
            thermalPrinter.setStep(12);
            thermalPrinter.print("       TOP UP e-PAY\n");
            thermalPrinter.setStep(12);
            for (int i = 0; i < 48; i++) buffer[i] = (byte) 0xfc;
            thermalPrinter.printLine(buffer);
            thermalPrinter.printLine(buffer);
            thermalPrinter.setStep(12);

            thermalPrinter.print("NPWRD   : " + idNPWRD + "\n");
            thermalPrinter.print("NAMA    : " + nama + "\n");
            int ints = Integer.parseInt(topup);
            NumberFormat n;
            n = NumberFormat.getCurrencyInstance();
            String topupformat = n.format(ints);
            topupformat = topupformat.replace("$", "");
            topupformat = topupformat.replace(".00", "");
            topupformat = topupformat.replace(",", ".");

            thermalPrinter.print("TOP UP  : Rp. " + topupformat + "\n");
            thermalPrinter.print("NO REFF : " + noreff + "\n");
            thermalPrinter.print("TGL/JAM : " + tgljam + "\n");
            ints = Integer.parseInt(saldoakhir);
            String saldoformat = n.format(ints);
            saldoformat = saldoformat.replace("$", "");
            saldoformat = saldoformat.replace(".00", "");
            saldoformat = saldoformat.replace(",", ".");

            thermalPrinter.print("SALDO   : Rp. " + saldoformat + "\n");
            thermalPrinter.setStep(12);
            thermalPrinter.printLine(buffer);
            thermalPrinter.printLine(buffer);
            thermalPrinter.setStep(12);
            thermalPrinter.print("         TERIMA KASIH\n");
            thermalPrinter.print(" STRUK INI SEBAGAI TANDA BUKTI");
            thermalPrinter.print("      PEMBAYARAN YANG SAH");
            thermalPrinter.setStep(200);
            thermalPrinter.printStart();
            thermalPrinter.waitForPrintFinish();

            print("       TOP UP e-PAY\n");
            print("NPWRD   : " + idNPWRD + "\n");
            print("NAMA    : " + nama + "\n");
            print("TOP UP  : Rp. " + topupformat + "\n");
            print("NO REFF : " + noreff + "\n");
            print("TGL/JAM : " + tgljam + "\n");
            print("SALDO   : Rp. " + saldoformat + "\n");
        }
        catch (Exception e){
            e.printStackTrace();
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            return false;
        } finally {
            return  true;
        }
    }
    public boolean printpayment(String kios,String nama,String retribusi,String noreff,String tgljam){
        try {

            JSONObject jsonObject = null;
            final StringBuffer sb = new StringBuffer();
            Locale localeID = new Locale("in", "ID");
            NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
            String idbilling=null, name=null, kecamatan=null, kelurahan=null, rwrt=null, nominal_pokok=null, nominal_denda=null, nominal_total=null;
            try {
                jsonObject = new JSONObject(kios);
                Log.e("++++ JSONOBJECT", jsonObject.toString());
                idbilling = "ID BILLING : " + jsonObject.getString("id_billing") + "\n";
                name = "NAMA : " + jsonObject.getString("nama") + "\n";
                kecamatan = "KECAMATAN : " + jsonObject.getString("kecamatan") + "\n";
                kelurahan = "KELURAHAN : " + jsonObject.getString("kelurahan") + "\n";
                rwrt = "RW/RT : " + jsonObject.getString("rw") + "/" + jsonObject.getString("rt") + "\n";
                nominal_pokok = "NOMINAL POKOK : " + formatRupiah.format(Double.valueOf(jsonObject.getString("nominal_pokok"))) + "\n";
                nominal_denda = "NOMINAL DENDA : " + formatRupiah.format(Double.valueOf(jsonObject.getString("nominal_denda"))) + "\n";
                nominal_total = "NOMINAL TOTAL : " + formatRupiah.format(Double.valueOf(jsonObject.getString("nominal_total"))) + "\n";
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("TES PRINT", kios);
            settings = new Settings(ActMain.this);
            HashMap<String, String> dev = settings.getSetting();
            String code = dev.get(Settings.CODE);
            thermalPrinter.initBuffer();
            thermalPrinter.setGray(2);
            Resources res = getResources();
            Bitmap bitmap1;
            try {
                if (BitmapFactory.decodeFile("/sdcard/logo_eSampah.jpg") != null) {
                    bitmap1 = BitmapFactory.decodeFile("/sdcard/logo_eSampah.jpg");
                } else bitmap1 = BitmapFactory.decodeResource(res, R.mipmap.logokota);
            } catch ( Exception e){
                e.printStackTrace();
                print(e.toString());
                bitmap1 = BitmapFactory.decodeResource(res, R.mipmap.logokota);
            }
            thermalPrinter.printLogo(0, 0, bitmap1);

            thermalPrinter.setHeightAndLeft(0, 0);
            thermalPrinter.setLineSpacing(5);
            thermalPrinter.setDispMode(ThermalPrinter.UMODE);
            thermalPrinter.setFont(ThermalPrinter.ASC12X24, ThermalPrinter.HZK12);
            thermalPrinter.setStep(12);
            thermalPrinter.print("   PEMERINTAH " + dbkota[Integer.parseInt(code)][2] + "\n");
            thermalPrinter.print("        RETRIBUSI SAMPAH\n");
            thermalPrinter.setStep(12);
            for (int i = 0; i < 48; i++) buffer[i] = (byte) 0xfc;
            thermalPrinter.printLine(buffer);
            thermalPrinter.printLine(buffer);
            thermalPrinter.setStep(12);
            //thermalPrinter.print("BLOK : " + kios + "\n");
            //thermalPrinter.print(kios);
            //String idbilling, name, kecamatan, kelurahan, rwrt, nominal_pokok, nominal_denda, nominal_total;
            thermalPrinter.print(idbilling);
            thermalPrinter.print(name);
            thermalPrinter.print(kecamatan);
            thermalPrinter.print(kelurahan);
            thermalPrinter.print(rwrt);
            thermalPrinter.print(nominal_pokok);
            thermalPrinter.print(nominal_denda);
            thermalPrinter.print(nominal_total);
            thermalPrinter.print("PEMILIK  : " + nama + "\n");
            int ints = Integer.parseInt(retribusi);
            NumberFormat n;
            n = NumberFormat.getCurrencyInstance();
            String retr = n.format(ints);
            retr = retr.replace("$", "");
            retr = retr.replace(".00", "");
            retr = retr.replace(",", ".");
            thermalPrinter.print("RETRIBUSI: Rp. " + retr + "\n");
            thermalPrinter.print("NO REFF  : " + noreff + "\n");
            thermalPrinter.print("TGL/JAM  : " + tgljam + "\n");

            thermalPrinter.setStep(12);
            thermalPrinter.printLine(buffer);
            thermalPrinter.printLine(buffer);
            thermalPrinter.setStep(12);
            thermalPrinter.print("         TERIMA KASIH\n");
            thermalPrinter.print(" STRUK INI SEBAGAI TANDA BUKTI");
            thermalPrinter.print("        PEMBAYARAN YANG SAH");
            thermalPrinter.setStep(200);
            thermalPrinter.printStart();
            thermalPrinter.waitForPrintFinish();

            print(" PEMERINTAH " + dbkota[Integer.parseInt(code)][2] + "\n");
            print("        RETRIBUSI PASAR\n");
            print("BLOK : " + kios + "\n");
            print("PEMILIK  : " + nama + "\n");
            print("RETRIBUSI: Rp. " + retr + "\n");
            print("NO REFF  : " + noreff + "\n");
            print("TGL/JAM  : " + tgljam + "\n");
        } catch (Exception e){
            e.printStackTrace();
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            return false;
        } finally {
            return  true;
        }

    }
    //    Button.OnClickListener f_saldo = new Button.OnClickListener(){
//        public void onClick(View v) {
//            readcard();
//        }
//    };
    Button.OnClickListener f_topup = new Button.OnClickListener(){
        public void onClick(View v) {
            topup();

        }
    };
    Button.OnClickListener f_setpass =new Button.OnClickListener(){
        public void onClick(View v) {
            changepass();
        }
    };
    Button.OnClickListener f_cekprint =new Button.OnClickListener(){
        public void onClick(View v) {
            if (printtest()) {;} else {msgbox("Cetak Gagal");};
        }
    };
    String BufferPayment="";

    public void getContactlessCard(int arg0, ContactlessCard arg1) {
        // TODO Auto-generated method stub
        if(arg0 == 0) {
            cardstate=1;
            stateString = "Successful";
            this.contactlessCard = arg1;
            Log.i(TAG,"Card Found");
            Message msg = new Message();
            msg.what = CHECK_SUCCESS;
            mHandler.sendMessage(msg);

        } else if(arg0 == PiccReader.TIMEOUT_ERROR) {
            cardstate=0;
            stateString = "Time out";
            Log.e(TAG,"Card Not Found");
            Message msg = new Message();
            msg.what = TIME_OUT;
            mHandler.sendMessage(msg);
        } else if(arg0 == PiccReader.USER_CANCEL) {
            cardstate=0;
            stateString = "User cancel";
            Log.e(TAG,"User cancel");
            Message msg = new Message();
            msg.what = USER_CANCEL;
            mHandler.sendMessage(msg);
        }
    }
    public void onBackPressed() {

        print("Back Pressed");
//        super.onBackPressed();
        int count = fm.getBackStackEntryCount();
        print("back count : "+count);
        handler.sendEmptyMessage(0);
        if (proses!=null && proses.isAlive()){handler.sendEmptyMessage(0);}
        else if (count==0) {
            print("logout?");
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("LOGOUT")
                    .setMessage("Keluar Aplikasi?")
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            timerHandler.removeCallbacks(timerRunnable);
                            exit=true;
//                            timerHandler = new Handler();
                            print("logout");
                            session.logoutUser();

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, 100);
                        }

                    })
                    .setNegativeButton("Tidak", null)
                    .show();
        } else {
            if (pagestate=="payment1"){
                pagestate="payment";
            }
            fm.popBackStack();

            return;
        }

    }
    @Override
//
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//
//    @Override


    @SuppressWarnings("StatementWithEmptyBody")
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Toast.makeText(getApplicationContext(), item.toString(), Toast.LENGTH_SHORT).show();
        if (id == R.id.cek_saldo) {
            print("menu check balance");
            pagestate="balance";
            //Set the fragment initially
            FragBalance fragment = new FragBalance();
            FragmentTransaction fragmentTransaction =
                    fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.top_up) {
            print("menu topup");
            pagestate="topup";
            FragTopup fragment = new FragTopup();
            FragmentTransaction fragmentTransaction =
                    fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();



        } else if (id == R.id.pay_ment) {
            print("menu payment");
            pagestate="payment";
            FragPay fragment = new FragPay();
            FragmentTransaction fragmentTransaction =
                    fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
//        } else if (id == R.id.nav_withdraw) {
//            FragWd fragment = new FragWd();
//            android.support.v4.app.FragmentTransaction fragmentTransaction =
//                    getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.replace(R.id.fragment_container, fragment);
//            fragmentTransaction.commit();

//        } else if (id == R.id.nav_conblue) {
//            Fragconblue fragment = new Fragconblue();
//            android.support.v4.app.FragmentTransaction fragmentTransaction =
//                    getSupportFragmentManager().beginTransaction();
//            fragmentTransaction.replace(R.id.fragment_container, fragment);
//            fragmentTransaction.commit();
        } else if (id == R.id.beranda) {
            print("menu home");
            pagestate="home";
            //Set the fragment initially
            FragMain fragment = new FragMain();
            FragmentTransaction fragmentTransaction =
                    fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.history) {
            print("menu history");
            pagestate="history";
            FragHistory fragment = new FragHistory();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }else if (id == R.id.inbox) {
            print("menu inbox");
            pagestate="inbox";
            FragInbox fragment = new FragInbox();
            android.support.v4.app.FragmentTransaction fragmentTransaction =
                    getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
        else if (id == R.id.setting) {
            print("menu setting");
            pagestate="repassword";
            //Set the fragment initially
            FragSetting fragment = new FragSetting();
            FragmentTransaction fragmentTransaction =
                    fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();

        }

        else if (id == R.id.nav_logout) {
            print("menu logout");
            session.logoutUser();
            //ActMain.ma.finish();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 100);
            //android.os.Process.killProcess(android.os.Process.myPid());
            // System.exit(1);

            // } //else if (id == R.id.nav_send) {

        }

//        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
//        Snackbar nsnackbar = Snackbar .make(coordinatorLayout, "Silakan Tap Kartu Untuk Mengetahui Saldo Kartu", Snackbar.LENGTH_LONG);
//        nsnackbar.show();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onUserInteraction() {
        print("user active");
        timerLogout.cancel();
        timerLogout = null;
        timerLogout = new MyCount(20*60*1000, 1);

        timerLogout.start();
        super.onUserInteraction();
    }
    @Override
    protected void onDestroy() {
        timerHandler.removeCallbacks(timerRunnable);
        exit=true;
//        timerHandler = new Handler();
//        if (picc.isAlive()) picc.stop();
        picc=null;
        // picccard=null;
        piccReader=null;
        Readerstate=0;
        print("logout");
        session.logoutUser();
        finish();
//        unregisterReceiver(receiver);
        super.onDestroy();
    }
    protected void onResume() {
        super.onResume();
        IntentFilter service01 = new IntentFilter(serviceReceiver.PING_SERVICE_DONE);
        service01.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver,service01);
        timerLogout.start();
        timerHandler.postDelayed(timerRunnable, 1000);

        try {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            }
            IMEI = tm.getDeviceId();
            USIM = tm.getSimSerialNumber();
        } catch (Exception e) {
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();

        }

        initprint();
        initpicc();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                dellog(65);
            }
        }, 10000);

        counttime=0;
    }


    public class serviceReceiver extends BroadcastReceiver {
        public static final String PING_SERVICE_DONE    ="com.bankjateng.eretjpr.intent.action.PING_SERVICE_DONE";
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(this.PING_SERVICE_DONE.equals(action)){
                double pingtime=Double.parseDouble(intent.getStringExtra(pingService.RESPONSE_TIME));
                String pingresult=intent.getStringExtra(pingService.RESPONSE_STRING);
                ImageView iv=(ImageView) findViewById(R.id.ivcon);
                if (pingtime<=200) {BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.mipmap.icongreen);
                    iv.setBackground(bg);

                } else if (pingtime>=200 && pingtime<=800) {
                    BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.mipmap.iconyellow);
                    iv.setBackground(bg);
                } else {
                    showtoast("Bad Internet Connection");
                    BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.mipmap.iconred);
                    iv.setBackground(bg);
                }
                print(pingresult);
            }
        }
    };
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
    public boolean isInternetAvailable(String address) {
        try {
            InetAddress ipAddr = InetAddress.getByName(address); //You can replace it with your name
            return !ipAddr.equals("");//InetAddress.getByName(address).isReachable(500);

        } catch (Exception e) {
            return false;
        }

    }
    boolean exit=false;
    int count=0;
    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void run() {
            if (exit==false && counttime%12==0) {
                (new Thread(new Runnable() {
                    @Override
                    public void run() {
                        settings =new Settings(getApplicationContext());
                        HashMap<String,String> dev=settings.getSetting();
                        String user = dev.get(Settings.USERNAME);
                        if (user!=null) GetEcho(user);
                    }
                })).start();
            }
            ImageView iv=(ImageView) findViewById(R.id.ivcon);
            if (isNetworkConnected()==false){
                print("Internet Connection not available");
                BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.mipmap.iconred);
                iv.setBackground(bg);
            } else if (isInternetAvailable(lip())==false) {
                print("Bad Internet Connection");
                BitmapDrawable bg = (BitmapDrawable)getResources().getDrawable(R.mipmap.iconred);
                iv.setBackground(bg);
            } else{
                Intent iS=new Intent(getApplicationContext(),pingService.class);
                iS.putExtra(pingService.REQUEST_STRING,lip());
                startService(iS);
            }
            if (exit==false) timerHandler.postDelayed(this, 5000);
            counttime++;
        }
    };

    protected void onPause() {
//        if (mBluetoothAdapter != null) {
//            if (mBluetoothAdapter.isDiscovering()) {
//                mBluetoothAdapter.cancelDiscovery();
//            }
//        }
//
//        if (mConnector != null) {
//            try {
//                mConnector.disconnect();
//            } catch (P25ConnectionException e) {
//                e.printStackTrace();
//            }
//        }

        // disabling foreground dispatch:
        //  NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        // nfcAdapter.disableForegroundDispatch(this);
        unregisterReceiver(receiver);
        timerHandler.removeCallbacks(timerRunnable);
        exit=true;
//        timerHandler = new Handler();
//        if (picc.isAlive())picc.stop();
        picc=null;
//        picccard=null;
        piccReader=null;
        Readerstate=0;
        finish();
        super.onPause();
    }
    public void msgbox(final String textmsg){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    new AlertDialog.Builder(ActMain.this)
                            .setTitle("Peringatan")
                            .setMessage(textmsg.toString())
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                }
                            })
                            .setCancelable(false)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                catch(Exception e){                        print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                    e.printStackTrace();}
            }
        });
    }
    public void printtopup2(final String idNPWRD,final String nama,final String topup,final String noreff,final String tgljam,final String saldoakhir){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    new AlertDialog.Builder(ActMain.this)
                            .setTitle("Transaksi Berhasil")
                            .setMessage("Cetak Struk Ulang")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if (printtopup(idNPWRD,nama,topup,noreff,tgljam,saldoakhir)) {;} else {msgbox("Cetak Gagal");};
                                }
                            })
                            .setCancelable(false)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                catch(Exception e){
                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                    e.printStackTrace();}
            }
        });
    }
    public void printpayment2(final String kios,final String nama,final String retribusi,final String noreff,final String tgljam){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    new AlertDialog.Builder(ActMain.this)
                            .setTitle("Transaksi Berhasil")
                            .setMessage("Cetak Struk Ulang")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    if ( printpayment(kios,nama,retribusi,noreff,tgljam)) {;} else {msgbox("Cetak Gagal");};
                                }
                            })
                            .setCancelable(false)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                catch(Exception e){
                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                    e.printStackTrace();}
            }
        });
    }
    ListView lvhistory;
    ListView lvmsg;
    public void getfilterhistory(final View v,String filter) {
        lvhistory = (ListView) v.findViewById(R.id.lvHistory);
        final DBHelper dbHelper = new DBHelper(getApplicationContext());
        final ArrayList<HashMap<String, String>> data =dbHelper.getFiltertrx(filter);

        print(data.toString());
        trxlistadapter adapter=new trxlistadapter(ActMain.this, data);
        lvhistory.setAdapter(adapter);
        // Click event for single list row
        lvhistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                if (thermalPrinter.isPaperOut()==false) {
//                        msgbox(Integer.toString(position)+""+Long.toString(id));
                    if (data.get(position).get(DBHelper.KEY_REFF).contains("D")){
                        final String idNPWRD=data.get(position).get(DBHelper.KEY_NPWRD);
                        final String topup=data.get(position).get(DBHelper.KEY_AMOUNT);
                        final String nama=data.get(position).get(DBHelper.KEY_NAME);
                        final String noreff=data.get(position).get(DBHelper.KEY_REFF);
                        final String tgljam=data.get(position).get(DBHelper.KEY_DATE);
                        final String saldoakhir=data.get(position).get(DBHelper.KEY_BAL);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    new AlertDialog.Builder(ActMain.this)
                                            .setTitle("Riwayat Transaksi")
                                            .setMessage("Apakah Anda Ingin Cetak Struk Ulang")
                                            .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (printtopup(idNPWRD,nama,topup,noreff,tgljam,saldoakhir)) {;} else {msgbox("Cetak Gagal");};
                                                }
                                            })
                                            .setCancelable(false)
                                            .setNegativeButton("TIDAK", null)
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }
                                catch(Exception e){
                                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                                    e.printStackTrace();}
                            }
                        });
                    } else if (data.get(position).get(DBHelper.KEY_REFF).contains("B")){
                        final String kios=data.get(position).get(DBHelper.KEY_INFO);
                        final String amount=data.get(position).get(DBHelper.KEY_AMOUNT);
                        final String nama=data.get(position).get(DBHelper.KEY_NAME);
                        final String noreff=data.get(position).get(DBHelper.KEY_REFF);
                        final String tgljam=data.get(position).get(DBHelper.KEY_DATE);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    new AlertDialog.Builder(ActMain.this)
                                            .setTitle("Riwayat Transaksi")
                                            .setMessage("Apakah Anda Ingin Cetak Struk Ulang")
                                            .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (printpayment(kios,nama,amount,noreff,tgljam)) {;} else {msgbox("Cetak Gagal");};
                                                }
                                            })
                                            .setCancelable(false)
                                            .setNegativeButton("TIDAK", null)
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }
                                catch(Exception e){
                                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                                    e.printStackTrace();

                                }
                            }
                        });
                    }
                } else {
                    msgbox("Isi Ulang Kertas dahulu\nsebelum melakukan Cetak");
                }
            }
        });
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, R.id.lstContent,dbHelper.getAlltrx() );
//        lvhistory.setAdapter(adapter);
    }
    public void getallmsg(View v) {
        lvmsg = (ListView) v.findViewById(R.id.lvmsg);
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        print(dbHelper.getAllmsg().toString());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, R.id.lstContent,dbHelper.getAllmsg() );
        lvmsg.setAdapter(adapter);
    }

    public void gettrxhistory(final View v) {
        lvhistory = (ListView) v.findViewById(R.id.lvHistory);
        final DBHelper dbHelper = new DBHelper(getApplicationContext());
        final ArrayList<HashMap<String, String>> data =dbHelper.getAlltrx();

        print(data.toString());
        trxlistadapter adapter=new trxlistadapter(ActMain.this, data);
        lvhistory.setAdapter(adapter);
        // Click event for single list row
        lvhistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                if (thermalPrinter.isPaperOut()==false) {
//                        msgbox(Integer.toString(position)+""+Long.toString(id));
                    if (data.get(position).get(DBHelper.KEY_REFF).contains("D")){
                        final String idNPWRD=data.get(position).get(DBHelper.KEY_NPWRD);
                        final String topup=data.get(position).get(DBHelper.KEY_AMOUNT);
                        final String nama=data.get(position).get(DBHelper.KEY_NAME);
                        final String noreff=data.get(position).get(DBHelper.KEY_REFF);
                        final String tgljam=data.get(position).get(DBHelper.KEY_DATE);
                        final String saldoakhir=data.get(position).get(DBHelper.KEY_BAL);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    new AlertDialog.Builder(ActMain.this)
                                            .setTitle("Riwayat Transaksi")
                                            .setMessage("Apakah Anda Ingin Cetak Struk Ulang")
                                            .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if ( printtopup(idNPWRD,nama,topup,noreff,tgljam,saldoakhir)) {;} else {msgbox("Cetak Gagal");};
                                                }
                                            })
                                            .setCancelable(false)
                                            .setNegativeButton("TIDAK", null)
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }
                                catch(Exception e){
                                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                                    e.printStackTrace();}
                            }
                        });
                    } else if (data.get(position).get(DBHelper.KEY_REFF).contains("B")){
                        final String kios=data.get(position).get(DBHelper.KEY_INFO);
                        final String amount=data.get(position).get(DBHelper.KEY_AMOUNT);
                        final String nama=data.get(position).get(DBHelper.KEY_NAME);
                        final String noreff=data.get(position).get(DBHelper.KEY_REFF);
                        final String tgljam=data.get(position).get(DBHelper.KEY_DATE);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    new AlertDialog.Builder(ActMain.this)
                                            .setTitle("Riwayat Transaksi")
                                            .setMessage("Apakah Anda Ingin Cetak Struk Ulang")
                                            .setPositiveButton("YA", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if ( printpayment(kios,nama,amount,noreff,tgljam)) {;} else {msgbox("Cetak Gagal");};
                                                }
                                            })
                                            .setCancelable(false)
                                            .setNegativeButton("TIDAK", null)
                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }
                                catch(Exception e){
                                    print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                                    e.printStackTrace();}
                            }
                        });
                    }
                } else {
                    msgbox("Isi Ulang Kertas dahulu\nsebelum melakukan Cetak");
                }
            }
        });
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.list_item, R.id.lstContent,dbHelper.getAlltrx() );
//        lvhistory.setAdapter(adapter);
    }
    protected void onNewIntent(Intent intent) {
//        if (intent.getAction().equals(NfcAdapter.ACTION_TAG_DISCOVERED)) {
//            Kartu = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//           // print( "Card detected");
//            View view;
//           // Toast.makeText(this, "Available Card Detected ID: " + ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)), Toast.LENGTH_SHORT).show();
//            //Toast.makeText(this, "Membaca Kartu ...", Toast.LENGTH_SHORT).show();
//            //tagID.setText("Card Tag ID : " + ByteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)));
//
//
//          //  nfcBalance();
//            Snackbar nsnackbar = Snackbar .make(coordinatorLayout, "Membaca Kartu ... ", Snackbar.LENGTH_SHORT);
//            nsnackbar.show();
//        }

    }

    private final String USER_AGENT = "Mozilla/5.0";
    public String spcchar(String Message){
        String buff=Message;
        buff=buff.replace("%","%25")
                .replace(" ","%20")
                .replace("\"","%22")
                .replace("-","%2D")
                // .replace(".","%2E")
                .replace("<","%3C")
                .replace(">","%3E")
                .replace("\\","%5C")
                .replace("^","%5E")
                .replace("_","%5F")
                .replace("`","%60")
                .replace("{","%7B")
                .replace("|","%7C")
                .replace("}","%7D")
                .replace("~","%7E")
                .replace("\n","%0D");
        return buff;
    }

    public String sendPost(String url,JSONObject object) throws Exception {

        //= "https://selfsolve.apple.com/wcResults.do";
        URL obj = new URL(url);
        final HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 60000);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);


        HttpPost postRequest = new HttpPost( url );

        StringEntity input = new StringEntity(object.toString());
        input.setContentType("application/json");
        postRequest.setEntity(input);

        HttpResponse httpresponse = httpClient.execute(postRequest);

        int responseCode = httpresponse.getStatusLine().getStatusCode();
        if (responseCode!=200) {print("ERROR "+Integer.toString(responseCode));}
        print(Integer.toString(responseCode));
//        System.out.println("\nSending 'POST' request to URL : " + url);
//        System.out.println("Post parameters : " + urlParameters);
//        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader((httpresponse.getEntity().getContent())));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        httpClient.getConnectionManager().shutdown();
        //print result
        //  System.out.println(response.toString());
        return(Integer.toString(responseCode)+"#"+response.toString()+"#");
    }

    public String sendGet(String url) throws Exception {

        //= "http://www.google.com/search?q=mkyong";

        URL obj = new URL(url);

        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setDoOutput(false);
        con.setConnectTimeout(60000);
        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();
        if (responseCode!=200) {print("ERROR "+Integer.toString(responseCode));}
        print(Integer.toString(responseCode));
        //  print( "\nSending 'GET' request to URL : " + url);
        //  print("Response Code : " + responseCode);
        //print("\nSending 'GET' request to URL : " + url);
        //print("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        //print result
        //      print(Integer.toString(responseCode)+"#"+response.toString()+"#");
        //  print(response.toString());
        return(Integer.toString(responseCode)+"#"+response.toString()+"#");
    }
    byte[] hexStringToByteArray(String s) {
        s.replace(" ","");
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
                    .digit(s.charAt(i + 1), 16));
        }
        return data;
    }
    String ByteArrayToHexString(byte [] inarray) {
        int i, j, in;

        String [] hex = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
        String out= "";
        for(j = 0 ; j < inarray.length ; ++j)
        {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out += hex[i];
            i = in & 0x0f;
            out += hex[i];
        }

        return out;
    }
    public static String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    private void msg(String s) { Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
    }
    public String[] getArray(ArrayList<BluetoothDevice> data) {
        String[] list = new String[0];

        if (data == null) return list;

        int size	= data.size();
        list		= new String[size];

        for (int i = 0; i < size; i++) {
            list[i] = data.get(i).getName();
        }

        return list;
    }

    void showtoast(final String msg){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ActMain.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getping(String url) {
        String str = "";
        try {
            //Thread.sleep(1000);
            Process process = Runtime.getRuntime().exec(
                    "/system/bin/ping -c 1 " + url);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    process.getInputStream()));

            int i;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((i = reader.read(buffer)) > 0 )    output.append(buffer, 0, i);
            reader.close();
            // body.append(output.toString()+"\n");
            //print(output);
            int start=output.toString().indexOf("time=");
            int stop=output.toString().indexOf(" ms");
            //print("Start " +start+ " Stop"+stop);
            if (start>0) {
                String time=output.toString().substring(start+5,stop);
                str = "Ping time : " + time + " ms";
                pingtime=Double.parseDouble(time);
            }  else {str= "Ping timeout/notreach";}
            // Log.d(TAG, str);
        } catch (Exception e) {
            // body.append("Error\n");
            print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
            e.printStackTrace();
        }
        pingresult=str;
    }
    String pingresult="";
    double pingtime=1000;

    public class MyCount extends CountDownTimer {
        int time = 0;
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            timerHandler.removeCallbacks(timerRunnable);
            exit=true;
//            timerHandler = new Handler();
            print("logout timeout");
            session.logoutUser();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 100);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (time%60000==0) print("timeleft "+(time/60000));
            time++;
        }
    }

    public static String fc_BytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    public String fc_HexToString(String hex) {
        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2) {

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }
        return sb.toString();
    }
    static final String [][] dbkota={{"000"," "," "}
            ,{"001"," "," "}
            ,{"002","3372","KOTA SURAKARTA"}
            ,{"003","3302","KABUPATEN BANYUMAS"}
            ,{"004","3376","KOTA TEGAL"}
            ,{"005","3371","KOTA MAGELANG"}
            ,{"006","3318","KABUPATEN PATI"}
            ,{"007","3375","KOTA PEKALONGAN"}
            ,{"008","3305","KABUPATEN KEBUMEN"}
            ,{"009","3310","KABUPATEN KLATEN"}
            ,{"010","3314","KABUPATEN SRAGEN"}
            ,{"011","3312","KABUPATEN WONOGIRI"}
            ,{"012","3301","KABUPATEN CILACAP"}
            ,{"013","3304","KABUPATEN BANJARNEGARA"}
            ,{"014","3323","KABUPATEN TEMANGGUNG"}
            ,{"015","3320","KABUPATEN JEPARA"}
            ,{"016","3316","KABUPATEN BLORA"}
            ,{"017","3315","KABUPATEN GROBOGAN"}
            ,{"018","3324","KABUPATEN KENDAL"}
            ,{"019","3313","KABUPATEN KARANGANYAR"}
            ,{"020","3306","KABUPATEN PURWOREJO"}
            ,{"021","3374","KOTA SEMARANG"}
            ,{"022","3322","KABUPATEN SEMARANG"}
            ,{"023","3307","KABUPATEN WONOSOBO"}
            ,{"024","3319","KABUPATEN KUDUS"}
            ,{"025","3327","KABUPATEN PEMALANG"}
            ,{"026","3309","KABUPATEN BOYOLALI"}
            ,{"027","3303","KABUPATEN PURBALINGGA"}
            ,{"028","3329","KABUPATEN BREBES"}
            ,{"029","3317","KABUPATEN REMBANG"}
            ,{"030","3311","KABUPATEN SUKOHARJO"}
            ,{"031","3321","KABUPATEN DEMAK"}
            ,{"032","3325","KABUPATEN BATANG"}
            ,{"033","3373","KOTA SALATIGA"}
            ,{"034"," "," "}
            ,{"035","3328","KABUPATEN TEGAL"}
            ,{"036"," "," "}
            ,{"037"," "," "}
            ,{"038"," "," "}
            ,{"039"," "," "}
            ,{"040"," "," "}
            ,{"041"," "," "}
            ,{"042"," "," "}
            ,{"043"," "," "}
            ,{"044"," "," "}
            ,{"045"," "," "}
            ,{"046"," "," "}
            ,{"047"," "," "}
            ,{"048"," "," "}
            ,{"049"," "," "}
            ,{"050"," "," "}
            ,{"051"," "," "}
            ,{"052"," "," "}
            ,{"053"," "," "}
            ,{"054"," "," "}
            ,{"055"," "," "}
            ,{"056"," "," "}
            ,{"057"," "," "}
            ,{"058"," "," "}
            ,{"059"," "," "}
            ,{"060"," "," "}
            ,{"061"," "," "}
            ,{"062"," "," "}
            ,{"063"," "," "}
            ,{"064"," "," "}
            ,{"065"," "," "}
            ,{"066"," "," "}
            ,{"067"," "," "}
            ,{"068"," "," "}
            ,{"069"," "," "}
            ,{"070"," "," "}
            ,{"071"," "," "}
            ,{"072"," "," "}
            ,{"073"," "," "}
            ,{"074"," "," "}
            ,{"075"," "," "}
            ,{"076"," "," "}
            ,{"077"," "," "}
            ,{"078"," "," "}
            ,{"079"," "," "}
            ,{"080"," "," "}
            ,{"081"," "," "}
            ,{"082"," "," "}
            ,{"083"," "," "}
            ,{"084"," "," "}
            ,{"085"," "," "}
            ,{"086"," "," "}
            ,{"087"," "," "}
            ,{"088"," "," "}
            ,{"089"," "," "}
            ,{"090"," "," "}
            ,{"091"," "," "}
            ,{"092"," "," "}
            ,{"093"," "," "}
            ,{"094"," "," "}
            ,{"095"," "," "}
            ,{"096"," "," "}
            ,{"097"," "," "}
            ,{"098"," "," "}
            ,{"099"," "," "}
            ,{"100"," "," "}
            ,{"101"," "," "}
            ,{"102"," "," "}
            ,{"103"," "," "}
            ,{"104"," "," "}
            ,{"105"," "," "}
            ,{"106"," "," "}
            ,{"107"," "," "}
            ,{"108","3308","KABUPATEN MAGELANG"}
            ,{"109","3326","KABUPATEN PEKALONGAN"}};
//    public class getPing implements Runnable {
//
//        Handler mHandler = new Handler(Looper.getMainLooper());
//
////        public GameHandler(Layout layout, View viewToAnimate, int speedRate) {
////            mLayout = layout;
////            mView = viewToAnimate;
////            mSpeedRate = speedRate;
////        }
//
//        public void ping() {
//            print(getping("google.com"));
//        }
//
//        private String getping(String url) {
//
//            String str = "";
//
//            try {
//                //Thread.sleep(1000);
//                Process process = Runtime.getRuntime().exec(
//                        "/system/bin/ping -c 8 " + url);
//                BufferedReader reader = new BufferedReader(new InputStreamReader(
//                        process.getInputStream()));
//                int i;
//                char[] buffer = new char[4096];
//                StringBuffer output = new StringBuffer();
//                while ((i = reader.read(buffer)) > 0)
//                    output.append(buffer, 0, i);
//                reader.close();
//                // body.append(output.toString()+"\n");
//                str = "ping : " + output.toString();
//                // Log.d(TAG, str);
//            } catch (Exception e) {
//                // body.append("Error\n");
//                e.printStackTrace();
//            }
//            return str;
//        }
//        void selectFlyingObject() {
//            // No idea what you're doing here, but keep in mind you're still working on the
//            // background thread if you call this method from somewhere not wrapped in the mHandler
//        }
//
//        @Override
//        public void run() {
//            ping();
//        }
//    }
}


