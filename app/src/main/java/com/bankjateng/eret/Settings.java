package com.bankjateng.eret;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

public class Settings {
	// Shared Preferences
	SharedPreferences pref;
	// Editor for Shared preferences
	Editor editor;
	// Context
	Context _context;

	// Shared pref mode
	int PRIVATE_MODE = 0;
	public static final String SavedBDev= "";
	public static final String USERNAME= "USERNAME";
	public static final String PASSWORD= "PASSWORD";
	public static final String CODE = "CODE";
	public static final String WS = "WS";
	public static final String TEL_NO= "TEL_NO";
	// Sharedpref file name
	private static final String PREF_NAME = "KJSDeviceSetting";


	// Constructor
	public Settings(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	
	/**
	 * Create login session
	 * */
	public void savedatauser(String USER, String PASS, String ws, String ID)
	{
		editor.putString(USERNAME, USER);
		editor.putString(PASSWORD, PASS);
		editor.putString(WS, ws);
		editor.putString(CODE, ID);
		editor.commit();
	}
	public void savebluetooth(String BluetoothDevices)
	{
		editor.putString(SavedBDev, BluetoothDevices);
		editor.commit();

	}
	/**
	 * Get stored session data
	 * */
	public HashMap<String,String> getSetting(){
		HashMap<String, String> bSet = new HashMap<String, String>();
		bSet.put(SavedBDev, pref.getString(SavedBDev,null));
		bSet.put(USERNAME, pref.getString(USERNAME,null));
		bSet.put(PASSWORD, pref.getString(PASSWORD,null));
		bSet.put(CODE, pref.getString(CODE,null));
		bSet.put(WS, pref.getString(WS,null));
		bSet.put(TEL_NO, pref.getString(TEL_NO,null));
		return bSet;
	}

	
	/**
	 * Clear session details
	 * */

}
