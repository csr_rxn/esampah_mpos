package com.bankjateng.eret;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.io.IOException;
import java.security.GeneralSecurityException;

import in.co.ophio.secure.core.KeyStoreKeyGenerator;
import in.co.ophio.secure.core.ObscuredPreferencesBuilder;

/**
 * Created by USER on 8/30/2017.
 */

public class PreferenceHelper {

    private static SharedPreferences mPref;
    private static SharedPreferences mSecurePref;

    public static SharedPreferences getSharedPreference(Context context){
        if(mPref == null)
            mPref = context.getSharedPreferences("example_pref", Context.MODE_PRIVATE);
        return mPref;
    }

    public static SharedPreferences getSecureSharedPreference(Application application, Context context){
        if(mSecurePref == null){
            String secureKey = null;
            try {
                secureKey = KeyStoreKeyGenerator.get(application,
                        application.getPackageName()).loadOrGenerateKeys();
            }catch (GeneralSecurityException | IOException e){
                Toast.makeText(context, "can't create key", Toast.LENGTH_SHORT).show();
                throw new RuntimeException("can't create  key");
            }

            mSecurePref = new ObscuredPreferencesBuilder()
                    .setApplication(application)
                    .obfuscateKey(true)
                    .obfuscateValue(true)
                    .setSharePrefFileName("example_secure")
                    .setSecret(secureKey)
                    .createSharedPrefs();

        }
        return mSecurePref;
    }


}
