package com.bankjateng.eret;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragBalance extends Fragment {

    public FragBalance() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ActMain actMain =(ActMain) getActivity();
        View v = inflater.inflate(R.layout.fragment_balance, container, false);

        TextView tvnama=(TextView) v.findViewById(R.id.tvNAMA);
        TextView tvkios=(TextView) v.findViewById(R.id.tvKIOS);
        TextView tvsaldo=(TextView) v.findViewById(R.id.tvSALDO);

        tvkios.setVisibility(View.GONE);
        tvsaldo.setVisibility(View.GONE);
        tvnama.setText("\nSilahkan Tempelkan Kartu\n");


        Button b_scan=(Button) v.findViewById(R.id.bScan);
//        b_scan.setOnClickListener(actMain.f_saldo);
        b_scan.setText("CEK SALDO");
        b_scan.setVisibility(View.GONE);
        return v;
    }


}
