package com.bankjateng.eret;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragHistory extends Fragment {

    Button b_list;
    static final String myTAG="NFC TEST";
    public FragHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final ActMain actMain = (ActMain) getActivity();

        final View v = inflater.inflate(R.layout.fragment_history, container, false);
        final EditText filter = (EditText) v.findViewById(R.id.tfilter);

        b_list=(Button) v.findViewById(R.id.bshowhistory);
        b_list.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View vi) {
                actMain.getfilterhistory(v,filter.getText().toString());        }
        });
        b_list.setVisibility(View.GONE);
        filter.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            public void onTextChanged(CharSequence stringVar, int start, int before, int count) {
                actMain.getfilterhistory(v,stringVar.toString());
            }
        });

        actMain.gettrxhistory(v);
        return v;

    }

}
