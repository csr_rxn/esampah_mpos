package com.bankjateng.eret;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import justtide.ThermalPrinter;

public class ActReg extends Activity {

	// Email, password edittext
	String ws = "";
	String code = "";
	EditText txtUser, txtPass, txtPass2;
	TextView tvUser, tvPass, tvPass2;
	// login button
	Button btnLogin;
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Session Manager Class
	SessionManager session;
	Settings settings;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reg);
		findViewById(R.id.loadingPanel).setVisibility(View.GONE);
		// Session Manager
		session = new SessionManager(getApplicationContext());
		settings = new Settings(getApplicationContext());
		// Email, Password input text
		tvUser = (TextView) findViewById(R.id.tvUsername);
		tvPass = (TextView) findViewById(R.id.tvPassword);
		tvPass2 = (TextView) findViewById(R.id.tvPassword2);
		txtUser = (EditText) findViewById(R.id.txtUsername);
		txtPass = (EditText) findViewById(R.id.txtPassword);
		txtPass2 = (EditText) findViewById(R.id.txtPassword2);
		// Login button

		btnLogin = (Button) findViewById(R.id.btnLogin);

		// Login button click event
		btnLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startDialog();
				proses = new Thread(new Runnable() {
					@Override
					public void run() {
						String ID = "";
						if (txtPass.getVisibility() == View.GONE && txtPass2.getVisibility() == View.GONE && txtUser.getText().toString().length() > 3 && btnLogin.getText().toString().equals("AKTIVASI")) {
							//	findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);
							String resp = register(txtUser.getText().toString());
							if (resp.equals("00")) {
								runOnUiThread(new Runnable() {
									public void run() {
										tvUser.setVisibility(View.VISIBLE);
										tvPass.setVisibility(View.VISIBLE);
										tvPass2.setVisibility(View.VISIBLE);
										txtUser.setVisibility(View.VISIBLE);
										txtPass.setVisibility(View.VISIBLE);
										txtPass2.setVisibility(View.VISIBLE);
										btnLogin.setText("LOGIN");
									}
								});

							} else {
								return;
							}
							;
						} else if (txtPass.getText().toString().length() > 0 &&
								txtPass2.getText().toString().length() > 0 &&
								txtUser.getText().toString().length() > 0 &&
								btnLogin.getText().toString().equals("LOGIN")) {

							String username = txtUser.getText().toString();
							String password = txtPass.getText().toString();
							String password2 = txtPass2.getText().toString();
							if (password.length() < 5 | password2.length() < 5) {
								msgbox("Password kurang dari 5 karakter");
								return;
							}
							if (password.equals(password2) == false) {
								msgbox("Password Baru yang dimasukkan tidak sama!");
								return;
							}
							String Resp = login(username);
							if (username.trim().length() > 0 && password.trim().length() > 0 && Resp.equals("-0") == false && Resp.equals("-1") == false) {
								if (password.equals(password2)) {
									settings.savedatauser(username, password, ws, code);
									session.createLoginSession(username, Resp);
									// Staring ActMain
									Intent i = new Intent(getApplicationContext(), ActMain.class);
									startActivity(i);
									finish();

								} else {
									// username / password doesn't match
									alert.showAlertDialog(ActReg.this, "Login gagal..", "Username/Password salah", false);
								}
							} else {
								// user didn't entered username or password
								// Show alert asking him to enter the details
								alert.showAlertDialog(ActReg.this, "Login gagal..", "Ketik username and password", false);
							}
						} else {
							showtoast("Isilah dan cek data-data hingga benar terlebih dulu");
						}
					}
				});
				proses.start();
				wait_dialogclose(proses);
			}
		});

	}

	public void wait_dialogclose(final Thread n) {
		(new Thread(new Runnable() {
			@Override
			public void run() {

				while (n.isAlive()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				handler.sendEmptyMessage(0);
				//  piccThread = null;
			}
		})).start();
	}

	static {
		System.loadLibrary("jtg");
	}

	protected void onResume() {
		txtUser = (EditText) findViewById(R.id.txtUsername);
		txtPass = (EditText) findViewById(R.id.txtPassword);
		txtPass2 = (EditText) findViewById(R.id.txtPassword2);
		txtUser.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

		txtPass.setText("");
		txtPass2.setText("");
		txtUser.setText("");
		initprint();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				try{
					initprint();
				} catch (Exception e) {
					e.printStackTrace();
					print(e.toString());
				}

			}
		}, 3000);
		if (printtest()) {;} else {msgbox("Cetak Gagal");};
		super.onResume();
	}

	public native String lip();

	public native String lport();

	public native String lurlreg();

	public native String lurllogin();

	public String register(String user) {
		try {
			String Url = "http://" + lip() + ":" + lport() + lurlreg();
			TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
				// TODO: Consider calling
				//    ActivityCompat#requestPermissions
				// here to request the missing permissions, and then overriding
				//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
				//                                          int[] grantResults)
				// to handle the case where the user grants the permission. See the documentation
				// for ActivityCompat#requestPermissions for more details.
				return "-1";
			}
			if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
				// TODO: Consider calling
				//    ActivityCompat#requestPermissions
				// here to request the missing permissions, and then overriding
				//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
				//                                          int[] grantResults)
				// to handle the case where the user grants the permission. See the documentation
				// for ActivityCompat#requestPermissions for more details.
				return "-2";
			}
			String IMEI = tm.getDeviceId();
			String USIM=tm.getSimSerialNumber();
			if (USIM==null){USIM="0";}
			ArrayList<String> rjs = new ArrayList<String>();
			JSONObject sjhttp = new JSONObject();
			sjhttp.put("username", user);
			sjhttp.put("serial_kartu", USIM);
			sjhttp.put("imei",IMEI);
			System.out.println(Url+sjhttp.toString());
			String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
			System.out.println(httpresp.toString());
			//System.out.println(Url+sjhttp.toString());
			if (httpresp.toString().contains("200#")){
				String[] res=httpresp.split("#");
				JSONObject rjhttp=new JSONObject(res[1]);
				//System.out.println(res[1]);
				Object jrespv;
				String respcode="";
				String desccode="";
				jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
				jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();

				if (respcode.equals("00")){
					String[] data=desccode.split(",");
					if (data.length<2) return "-0";
					ws=data[0];
					code=data[1];
					//showtoast(desccode);
					return respcode;
				}
				else {

					msgbox(respcode+":"+desccode);
					return "-0";}
			} else  {
				msgbox("Tidak dapat terhubung ke Server");
				return"-0";
			}

		} catch (Exception e) {
			if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
			msgbox("Tidak dapat terhubung ke Server"); 	return"-0";
		}

	}
	ThermalPrinter thermalPrinter = ThermalPrinter.getInstance();
	public boolean printtest(){
		try {
			TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
				// TODO: Consider calling
				//    ActivityCompat#requestPermissions
				// here to request the missing permissions, and then overriding
				//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
				//                                          int[] grantResults)
				// to handle the case where the user grants the permission. See the documentation
				// for ActivityCompat#requestPermissions for more details.

			}
			if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
				// TODO: Consider calling
				//    ActivityCompat#requestPermissions
				// here to request the missing permissions, and then overriding
				//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
				//                                          int[] grantResults)
				// to handle the case where the user grants the permission. See the documentation
				// for ActivityCompat#requestPermissions for more details.

			}
			String IMEI = tm.getDeviceId();
			String USIM = tm.getSimSerialNumber();
			Calendar c = Calendar.getInstance();
			SimpleDateFormat dtm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String tgljam = dtm.format(c.getTime());

			thermalPrinter.initBuffer();
			thermalPrinter.setGray(2);
			Resources res = getResources();
			Bitmap bitmap1 = BitmapFactory.decodeResource(res, R.mipmap.logo);
			thermalPrinter.printLogo(0, 0, bitmap1);

			thermalPrinter.setHeightAndLeft(0, 0);
			thermalPrinter.setLineSpacing(5);
			thermalPrinter.setDispMode(ThermalPrinter.UMODE);
			//thermalPrinter.setFont(ThermalPrinter.ASC12X24,ThermalPrinter.HZK12);
			thermalPrinter.setFont(ThermalPrinter.ASC12X24, ThermalPrinter.HZK12);
			thermalPrinter.setStep(12);
			thermalPrinter.print("TEST CETAK e-PAY\n");
			thermalPrinter.print("IMEI : " + IMEI + "\n");
			thermalPrinter.print("SIM  : " + USIM + "\n");
			thermalPrinter.print("TGL/JAM : " + tgljam + "\n");

			thermalPrinter.setStep(200);
			thermalPrinter.printStart();
			thermalPrinter.waitForPrintFinish();

			print(" TEST CETAK e-PAY\n");
			print(" IMEI : " + IMEI + "\n");
			print(" SIM  : " + USIM + "\n");
			print(" TGL/JAM : " + tgljam + "\n");
		} catch (Exception e){
			e.printStackTrace();
			print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
			return false;
		} finally {
			return  true;
		}
	}
	protected static final int REFRESH_TICKET_TEXT = 0;
	protected static final int ERROR = 2;
	private byte[] buffer;
	private Handler mHandler = null;
	public void initprint(){
		buffer = new byte[48];
		mHandler = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == REFRESH_TICKET_TEXT) {
					//ticketText.setText(ticketString);
					// showStateText.setText(stateString);
				} else if (msg.what == ERROR) {
					// showStateText.setText(stateString);
				}
				super.handleMessage(msg);
			}
		};
	}
	public String login(String user){
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;
			String Url="http://"+lip()+":"+lport()+lurllogin();

			ArrayList<String> rjs = new ArrayList<String>();
			JSONObject sjhttp = new JSONObject();
			sjhttp.put("username", user);
			sjhttp.put("versi_software", version);

			System.out.println(Url+sjhttp.toString());
			String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
			System.out.println(httpresp.toString());
			//System.out.println(Url+sjhttp.toString());
			if (httpresp.toString().contains("200#")){
				String[] res=httpresp.split("#");
				JSONObject rjhttp=new JSONObject(res[1]);
				//System.out.println(res[1]);
				Object jrespv;
				String respcode="";
				String desccode="";
				jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
				jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();

				if (respcode.equals("00")){
					return desccode;
				}
				else {
					msgbox(respcode+":"+desccode);
					return "-0";}
			} else  {
				msgbox("Tidak dapat terhubung ke Server");
				return"-0";
			}

		} catch (Exception e) {
			if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
			msgbox("Tidak dapat terhubung ke Server"); return"-0";

		}

	}
	public void msgbox(final String textmsg){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try {

					new AlertDialog.Builder(ActReg.this)
							.setTitle("Peringatan")
							.setMessage(textmsg.toString())
							.setPositiveButton("OK", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									// continue with delete
								}
							})
							.setIcon(android.R.drawable.ic_dialog_alert)
							.show();
				}
				catch(Exception e){print(e.toString());}
			}
		});
	}
	Thread proses;
	void print(String msg){
		System.out.println(msg);
	}
	private ProgressDialog pd;
	private void startDialog()  {			pd = ProgressDialog.show(this, null, "Mohon Tunggu ... ");
	}
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if(pd!=null) pd.dismiss();
		}
	};

	void showtoast(final String msg){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
			Toast.makeText(ActReg.this, msg, Toast.LENGTH_LONG).show();
			}
		});
	}
	@Override
	public void onBackPressed() {

		super.onBackPressed();

		finish();
		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(1);

	}

	private final String USER_AGENT = "Mozilla/5.0";
	public String spcchar(String Message){
		String buff=Message;
		buff=buff.replace("%","%25")
				.replace(" ","%20")
				.replace("\"","%22")
				.replace("-","%2D")
				// .replace(".","%2E")
				.replace("<","%3C")
				.replace(">","%3E")
				.replace("\\","%5C")
				.replace("^","%5E")
				.replace("_","%5F")
				.replace("`","%60")
				.replace("{","%7B")
				.replace("|","%7C")
				.replace("}","%7D")
				.replace("~","%7E")
				.replace("\n","%0D");
		return buff;
	}
	public String sendGet(String url) throws Exception {

		//= "http://www.google.com/search?q=mkyong";

		URL obj = new URL(url);

		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setConnectTimeout(5000);
		con.setReadTimeout(20000);

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
        if (responseCode!=200) {System.out.println("ERROR "+Integer.toString(responseCode));}
        System.out.println(Integer.toString(responseCode));

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//      System.out.println(Integer.toString(responseCode)+"#"+response.toString()+"#");
		//  System.out.println(response.toString());
		return(Integer.toString(responseCode)+"#"+response.toString()+"#");

	}



}