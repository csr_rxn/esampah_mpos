package com.bankjateng.eret;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.prefs.Preferences;

public class ActLogin extends Activity {
	Preferences prefs = Preferences.userNodeForPackage(ActLogin.class);
	SQLiteDatabase db;
	int counttime;
	// Email, password edittext
	EditText txtUsername, txtPassword;

	// login button
	Button btnLogin;

	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();

	// Session Manager Class
	SessionManager session;
	Settings settings;
	static{
		System.loadLibrary("jtg");
	}

	public native String lip();
	public native String lport();
	public native String lurllogin();
	public native String lurlecho();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		counttime=0;
		//findViewById(R.id.loadingPanel).setVisibility(View.GONE);
		// Session Manager
		session = new SessionManager(getApplicationContext());

		// Email, Password input text
		txtUsername = (EditText) findViewById(R.id.txtUsername);
		txtPassword = (EditText) findViewById(R.id.txtPassword);
		txtUsername.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
		//   Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();
		db = openOrCreateDatabase(DBHelper.DATABASE_NAME,android.content.Context.MODE_PRIVATE ,null);

		// Login button
		btnLogin = (Button) findViewById(R.id.btnLogin);

		// Login button click event
		btnLogin.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				startDialog();
				proses = new Thread(new Runnable() {
					@Override
					public void run() {
						// Get username, password from EditText
						settings =new Settings(getApplicationContext());
						HashMap<String,String> dev=settings.getSetting();
						String user=dev.get(Settings.USERNAME);
						String pass=dev.get(Settings.PASSWORD);
						String username = txtUsername.getText().toString();
						String password = txtPassword.getText().toString();
						//only for testing purpose
//						if ( (user==null) && username.equals("admin")==true && password.equals("admin")==true  ) {
//							prefs.put("user", username);
//							session.createLoginSession(username, "001001,002001,002002,");
//							handler.sendEmptyMessage(0);
//							Intent i = new Intent(getApplicationContext(), ActMain.class);
//							startActivity(i);
//
//							finish();
//							return;
//						}
						if(username.trim().length() > 0 && password.trim().length() > 0 && username.equals(user) && password.equals(pass)  ){

							String Resp=login(username);

							if (Resp.equals("-0")==false && Resp.equals("-1")==false  ) {
								prefs.put("user", username);
								session.createLoginSession(username, Resp);
								handler.sendEmptyMessage(0);

								Intent i = new Intent(getApplicationContext(), ActMain.class);
								timerHandler.removeCallbacks(timerRunnable);
								exit=true;
//						timerHandler = new Handler();
								startActivity(i);
								finish();

							}else {
								// username / password doesn't match
								handler.sendEmptyMessage(0);
							}

						} else if ( username.equals(user)==false | password.equals(pass)==false ){
							msgbox("Username/Password salah");
							handler.sendEmptyMessage(0);
						}

						else{
							// user didn't entered username or password
							// Show alert asking him to enter the details
							handler.sendEmptyMessage(0);

						}

					}
				});
				proses.start();
				wait_dialogclose(proses);
			}
		});

	}
	public void wait_dialogclose(final Thread n){
		(new Thread(new Runnable() {
			@Override
			public void run() {

				while(n.isAlive()) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				handler.sendEmptyMessage(0);
				//  piccThread = null;
			}
		})).start();
	}
	public String GetEcho(String user){
		//http://192.168.66.109:81/smartcard/getBalance/{"trxcod":"004905","merchant":"4565","uid":"12345","reader":"987654321","hp":"085640301325","va_acc":"301160112360023"}
		String Url="http://"+lip()+":"+lport()+lurlecho();
		String nominal="";
		try {
			ArrayList<String> rjs = new ArrayList<String>();
			JSONObject sjhttp = new JSONObject();

			sjhttp.put("username", user);
			sjhttp.put("IMEI", IMEI);
			sjhttp.put("USIM", USIM);
			System.out.println(Url+sjhttp.toString());
			String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
			System.out.println(httpresp.toString());
			if (httpresp.toString().contains("200#")){
				String[] res=httpresp.split("#");

				JSONObject rjhttp=new JSONObject(res[1]);
				int lrjhttp=rjhttp.length();
				// System.out.println(res[1]);
				Object jrespv;
				String respcode="";
				String desccode="";
				jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
				jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();

				if (respcode.equals("00")){
					if (desccode.equals("Sukses")==false) {
						DBHelper dbHelper=new DBHelper(getApplicationContext());
						if (desccode.equals(dbHelper.getlastmsg())==false){
							Calendar c = Calendar.getInstance();
							SimpleDateFormat iddata = new SimpleDateFormat("yyyyMMddHHmmss");
							SimpleDateFormat dtm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
							dbHelper.inmsg(iddata.format(c.getTime()),dtm.format(c.getTime()),desccode);
							addNotification("e-Ret Jepara", desccode);
						}
					}
					return "";
				}
				else {
					showtoast(respcode+":"+desccode);
					return "-0";}
			} else  {
				if (httpresp.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
				//msgbox("Tidak dapat terhubung ke Server");
				return"-0";
			}

		} catch (Exception e) {
			System.out.println(e.toString());
			if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
			// msgbox("Data Tidak ketemu atau tidak terhubung ke server");
			return"-0";
		}
		// cardbal.setText("Saldo :");
		//cardno.setText("No.     :");
	}
	String IMEI=" ";
	String USIM=" ";
	public String login(String user){
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;
			String Url="http://"+lip()+":"+lport()+lurllogin();

			ArrayList<String> rjs = new ArrayList<String>();
			JSONObject sjhttp = new JSONObject();
			sjhttp.put("username", user);
			sjhttp.put("versi_software", version);
			sjhttp.put("IMEI", IMEI);
			sjhttp.put("USIM", USIM);
			System.out.println(Url+sjhttp.toString());
			String httpresp=sendGet(Url+spcchar(sjhttp.toString()));
			System.out.println(httpresp.toString());
			// System.out.println(Url+sjhttp.toString());
			if (httpresp.toString().contains("200#")){
				String[] res=httpresp.split("#");
				JSONObject rjhttp=new JSONObject(res[1]);
				//System.out.println(res[1]);
				Object jrespv;
				String respcode="";
				String desccode="";
				jrespv = rjhttp.get("resp_code");        if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) respcode=jrespv.toString();
				jrespv = rjhttp.get("resp_desc");       if (jrespv != null && jrespv.toString().contains("null")==false && jrespv.toString().length()>0) desccode=jrespv.toString();

				if (respcode.equals("00")){
					return desccode;
				}
				else {
					msgbox(respcode+":"+desccode);
					return "-0";}
			} else  {
				msgbox("Tidak dapat terhubung ke Server");
				return"-0";
			}

		} catch (Exception e) {
			if (e.toString().contains("Timeout")) {showtoast("Server Tidak Merespon");return "-1";}
			msgbox("Tidak dapat terhubung ke Server"); return"-0";

		}

	}
	private void addNotification(final String title,final String msg) {
		NotificationCompat.Builder builder =
				new NotificationCompat.Builder(this)
						.setSmallIcon(R.mipmap.icobjtg)
						.setContentTitle(title)
						.setContentText(msg);

		Intent notificationIntent = new Intent(this,ActLogin.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(contentIntent);
		builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		// Add as notification
		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		manager.notify(0, builder.build());
	}
	void showtoast(final String msg){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(ActLogin.this, msg, Toast.LENGTH_LONG).show();
			}
		});
	}
	@Override
	public void onBackPressed() {

		super.onBackPressed();
		finish();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);

	}
	Thread proses;
	private ProgressDialog pd;
	private void startDialog()  {
		pd = ProgressDialog.show(ActLogin.this, null, "Mohon Tunggu ... ");
		//start a new thread to process job

	}
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if(pd!=null) pd.dismiss();
		}
	};
	@Override
	protected void onPause() {
		super.onPause();
		//timerHandler.removeCallbacks(timerRunnable);
		//finish();
	}

	@Override
	protected void onDestroy() {
		timerHandler.removeCallbacks(timerRunnable);
		exit=true;
//		timerHandler = new Handler();
		finish();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		txtUsername = (EditText) findViewById(R.id.txtUsername);
		txtPassword = (EditText) findViewById(R.id.txtPassword);

		Calendar c= Calendar.getInstance();
		Date date = c.getTime();
		c.setTime ( date ); // convert your date to Calendar object
		int daysToDecrement = -30;
		c.add(Calendar.DATE, daysToDecrement);
		// again get back your date object
		SimpleDateFormat iddata = new SimpleDateFormat("yyyyMMdd");
		String tanggal= iddata.format(c.getTime());
		System.out.println(tanggal);
//		if (tanggal.equals("20180131") ||
//				tanggal.equals("20180201") ||
//				tanggal.equals("20180202") ||
//				tanggal.equals("20180203") ||
//				tanggal.equals("20180212") ||
//				tanggal.equals("20180213") ||
//				tanggal.equals("20180214")
//				) {
		settings =new Settings(getApplicationContext());
		HashMap<String,String> dev=settings.getSetting();
		String user=dev.get(Settings.USERNAME);
		String pass=dev.get(Settings.PASSWORD);
		txtPassword.setText(pass);
		txtUsername.setText(user);
//		} else {
//			txtPassword.setText("");
//			txtUsername.setText("");
//		}
		try {
			TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
			if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			}
			if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			}
			IMEI = tm.getDeviceId();
			USIM = tm.getSimSerialNumber();
		} catch (Exception e) {
			print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
			e.printStackTrace();

		}
		timerHandler.postDelayed(timerRunnable, 1000);
		counttime=0;
		super.onResume();

	}
	void print(String msg){
		Calendar c = Calendar.getInstance();
		SimpleDateFormat dtm = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String tgljam = dtm.format(c.getTime());
		System.out.println(tgljam+" : "+msg);
	}
	Boolean exit=false;
	Handler timerHandler = new Handler();
	Runnable timerRunnable = new Runnable() {

		@Override
		public void run() {
			if (exit==false && counttime%12==0) {
				(new Thread(new Runnable() {
					@Override
					public void run() {
						settings =new Settings(getApplicationContext());
						HashMap<String,String> dev=settings.getSetting();
						String user = dev.get(Settings.USERNAME);
						if (user!=null) GetEcho(user);
					}
				})).start();
			}
			if (exit==false) timerHandler.postDelayed(this, 5000);
			counttime++;
		}
	};
	public void msgbox(final String textmsg){
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				new AlertDialog.Builder(ActLogin.this)
						.setTitle("Peringatan")
						.setMessage(textmsg.toString())
						.setPositiveButton("OK", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// continue with delete
							}
						})
						.setIcon(android.R.drawable.ic_dialog_alert)
						.show();
			}
		});
	}

	private final String USER_AGENT = "Mozilla/5.0";

	public String sendPost(String url,JSONObject object) throws Exception {

		//= "https://selfsolve.apple.com/wcResults.do";
		URL obj = new URL(url);
		final HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 60000);

		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);


		HttpPost postRequest = new HttpPost( url );

		StringEntity input = new StringEntity(object.toString());
		input.setContentType("application/json");
		postRequest.setEntity(input);

		HttpResponse httpresponse = httpClient.execute(postRequest);

		int responseCode = httpresponse.getStatusLine().getStatusCode();
		if (responseCode!=200) {print("ERROR "+Integer.toString(responseCode));}
		print(Integer.toString(responseCode));
//        System.out.println("\nSending 'POST' request to URL : " + url);
//        System.out.println("Post parameters : " + urlParameters);
//        System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader((httpresponse.getEntity().getContent())));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		httpClient.getConnectionManager().shutdown();
		//print result
		//  System.out.println(response.toString());
		return(Integer.toString(responseCode)+"#"+response.toString()+"#");
	}

	public String sendGet(String url) throws Exception {

		//= "http://www.google.com/search?q=mkyong";

		URL obj = new URL(url);

		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setDoOutput(false);
		con.setConnectTimeout(60000);
		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		if (responseCode!=200) {print("ERROR "+Integer.toString(responseCode));}
		print(Integer.toString(responseCode));
		//  print( "\nSending 'GET' request to URL : " + url);
		//  print("Response Code : " + responseCode);
		//print("\nSending 'GET' request to URL : " + url);
		//print("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		//print result
		//      print(Integer.toString(responseCode)+"#"+response.toString()+"#");
		//  print(response.toString());
		return(Integer.toString(responseCode)+"#"+response.toString()+"#");
	}
	public String spcchar(String Message){
		String buff=Message;
		buff=buff.replace("%","%25")
				.replace(" ","%20")
				.replace("\"","%22")
				.replace("-","%2D")
				// .replace(".","%2E")
				.replace("<","%3C")
				.replace(">","%3E")
				.replace("\\","%5C")
				.replace("^","%5E")
				.replace("_","%5F")
				.replace("`","%60")
				.replace("{","%7B")
				.replace("|","%7C")
				.replace("}","%7D")
				.replace("~","%7E")
				.replace("\n","%0D");
		return buff;
	}
}
