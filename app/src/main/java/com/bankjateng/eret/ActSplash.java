package com.bankjateng.eret;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import java.util.HashMap;

public class ActSplash extends Activity {

	// Splash screen timerLogout
	private static int SPLASH_TIME_OUT = 3000;
	Settings settings;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		setContentView(R.layout.activity_splash);


		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try{
					PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
					String version = pInfo.versionName;
					((TextView) findViewById(R.id.ever)).setText(getApplicationContext().getResources().getString(R.string.app_name2)+" "+version);
				} catch (Exception e){
					System.out.println(e.getStackTrace().toString());
				}
			}
		});

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timerLogout. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timerLogout is over
				// Start your app main activity
				Intent i;
				settings =new Settings(getApplicationContext());
				HashMap<String,String> dev=settings.getSetting();
				String user=dev.get(Settings.USERNAME);
				String pass=dev.get(Settings.PASSWORD);

				if ( user==null || pass==null) {
					i = new Intent(ActSplash.this, ActReg.class);
					startActivity(i);
					System.out.println("Enter Register");
					finish();
				}
				else if (user!=null && pass!=null)
				{
					i = new Intent(ActSplash.this, ActLogin.class);
					startActivity(i);
					System.out.println("Enter Login");
					finish();
				}
//				i = new Intent(ActSplash.this, ActLogin.class);
//				startActivity(i);
//				System.out.println("Enter Login");
//				finish();
				// close this activity
				//finish();
			}
		}, SPLASH_TIME_OUT);
	}

}


