package com.bankjateng.eret;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragSetting extends Fragment {

    public FragSetting() {
        // Required empty public constructor
    }
    Settings settings;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final ActMain actMain =(ActMain) getActivity();
        View v = inflater.inflate(R.layout.fragment_setting, container, false);


        Button bmcetak;
        Button bpcetak;
        bmcetak = (Button) v.findViewById(R.id.bmcetak);
        bpcetak = (Button) v.findViewById(R.id.bpcetak);


        final int jmlcetak = Integer.parseInt( PreferenceHelper.getSecureSharedPreference(actMain.getApplication(), getContext())
                .getString("jmlcetak", "2"));

        final EditText tcetak;
        tcetak=(EditText) v.findViewById(R.id.txtcetak);
        tcetak.setInputType(InputType.TYPE_NULL);
        tcetak.setTextIsSelectable(true);
        tcetak.setText(String.valueOf(jmlcetak));

        tcetak.setOnTouchListener(new View.OnTouchListener(){
            public boolean onTouch(View view, MotionEvent motionEvent) {
                tcetak.setText("");
                //                // your code here....
//                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                return false;
            }
        });
        bmcetak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tcetak.getText().length()==0) tcetak.setText("0");
                int i=Integer.parseInt(tcetak.getText().toString());
                if(i>0) {i--; tcetak.setText(String.valueOf(i));}
            }
        });
        bpcetak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tcetak.getText().length()==0) tcetak.setText("0");
                int i=Integer.parseInt(tcetak.getText().toString());
                if(i<1000) {i++; tcetak.setText(String.valueOf(i));}
            }
        });
        Button bpass=(Button) v.findViewById(R.id.btnpass);
        bpass.setOnClickListener(actMain.f_setpass);
        Button btes=(Button) v.findViewById(R.id.btntes);
        btes.setOnClickListener(actMain.f_cekprint);

        Button bsaverate;

        bsaverate=(Button) v.findViewById(R.id.bsavecetak);
        bsaverate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String jml = tcetak.getText().toString();
                    if (jml.length() < 1) {
                        tcetak.setText("0");
                    }
                    PreferenceHelper.getSecureSharedPreference(actMain.getApplication(), actMain.getApplicationContext()).edit()
                            .putString("jmlcetak", jml).apply();
                    if ( PreferenceHelper.getSecureSharedPreference(actMain.getApplication(), actMain.getApplicationContext())
                                    .getString("jmlcetak", "").equals(jml)
                            ) {
                        actMain.msgbox("Ganti Jumlah Cetak Sukses");
                    } else {
                        actMain.msgbox("Ganti Jumlah Cetak Gagal");
                    }
                } catch(Exception e){
                    actMain.print("("+Thread.currentThread().getStackTrace()[1].getLineNumber()+")"+e.toString());
                    e.printStackTrace();};
            }
        });

        return v;
    }



}
