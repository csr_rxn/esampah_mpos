package com.bankjateng.eret;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */

public class FragMain extends Fragment {
    SessionManager session;
    public FragMain() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ActMain actMain =(ActMain) getActivity();
        session = new SessionManager(actMain);
        HashMap<String, String> user = session.getUserDetails();
        String name = user.get(SessionManager.KEY_NAME);
        String menus = user.get(SessionManager.KEY_EMAIL);
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        TextView tvnama=(TextView) v.findViewById(R.id.tvNAMA);
        TextView tvkios=(TextView) v.findViewById(R.id.tvKIOS);
        TextView tvsaldo=(TextView) v.findViewById(R.id.tvSALDO);

        tvkios.setVisibility(View.GONE);
        tvsaldo.setVisibility(View.GONE);
        tvnama.setText("\nSelamat Datang "+name+"\nUntuk Memulai Transaksi\nPilih menu navigasi\ndi Kiri Pojok Atas\n");

        Button b_scan=(Button) v.findViewById(R.id.bScan);
//        b_scan.setOnClickListener(actMain.f_saldo);
        b_scan.setText("CEK SALDO");
        b_scan.setVisibility(View.GONE);
        return v;
    }


}
