package com.bankjateng.eret;

import android.app.IntentService;
import android.content.Intent;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class pingService extends IntentService {
    public static final String REQUEST_STRING = "myRequest";

    public static final String RESPONSE_STRING = "myResponse";
    public static final String RESPONSE_TIME = "myResponse";

    public pingService(){
        super("pingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String requestString = intent.getStringExtra(REQUEST_STRING);
        String pingtime="1000";
        String pingresult="Ping timeout/notreach";
        String str = "";
        try {
            //Thread.sleep(1000);
            Process process = Runtime.getRuntime().exec(
                    "/system/bin/ping -c 1 " + requestString);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    process.getInputStream()));
            int i;
            char[] buffer = new char[4096];
            StringBuffer output = new StringBuffer();
            while ((i = reader.read(buffer)) > 0 )    output.append(buffer, 0, i);
            reader.close();
            // body.append(output.toString()+"\n");
            //System.out.println(output);
            int start=output.toString().indexOf("time=");
            int stop=output.toString().indexOf(" ms");
            //System.out.println("Start " +start+ " Stop"+stop);
            if (start>0) {
                String time=output.toString().substring(start+5,stop);
                str = "Ping time : " + time + " ms";
                pingtime=time;
            }  else {str= "Ping timeout/notreach";}
            // Log.d(TAG, str);
        } catch (Exception e) {
            // body.append("Error\n");
            System.out.println(e.toString());
        }
        pingresult=str;
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(ActMain.serviceReceiver.PING_SERVICE_DONE);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(RESPONSE_STRING, pingresult);
        broadcastIntent.putExtra(RESPONSE_TIME, pingtime);
        sendBroadcast(broadcastIntent);
    }


}
