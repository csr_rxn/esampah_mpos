package com.bankjateng.eret;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by aliexdsg on 8/18/2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "db_name.db";
    public static final String TABLE_NAME = "trxhistory";
    public static final String TABLE_NAME2 = "inbox";
    static final String KEY_ID = "id"; // parent node
    static final String KEY_NPWRD = "npwrd"; // parent node
    static final String KEY_NAME = "name";
    static final String KEY_AMOUNT = "amount";
    static final String KEY_REFF = "reff";
    static final String KEY_DATE = "date";
    static final String KEY_BAL = "bal";
    static final String KEY_INFO = "info";
    private HashMap hp;
    SQLiteDatabase mydatabase;
    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table " +
                        TABLE_NAME + "(id INTEGER PRIMARY KEY, " +
                        KEY_REFF+" TEXT , " +
                        KEY_NAME +" TEXT, " +
                        KEY_AMOUNT+" INTEGER, " +
                        KEY_BAL+" INTEGER, " +
                        KEY_NPWRD+" TEXT, " +
                        KEY_INFO+" TEXT, " +
                        KEY_DATE+" DATETIME)"
        );
        db.execSQL(
                "create table " +
                        TABLE_NAME2 + "(id INTEGER PRIMARY KEY, " +
                        KEY_INFO+" TEXT, " +
                        KEY_DATE+" DATETIME)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS  " +TABLE_NAME+
                " ");
        db.execSQL("DROP TABLE IF EXISTS  " +TABLE_NAME2+
                " ");
        onCreate(db);
    }
    public boolean inmsg(String id,  String date, String info)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ID, id);
        contentValues.put(KEY_DATE, date);
        contentValues.put(KEY_INFO, info);
        db.insert(TABLE_NAME2, null, contentValues);
        return true;
    }
    public boolean intrx(String id, String reff, String name, String amount, String bal, String npwrd, String date, String info)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ID, id);
        contentValues.put(KEY_REFF, reff);
        contentValues.put(KEY_NAME, name);
        contentValues.put(KEY_AMOUNT, amount);
        contentValues.put(KEY_NPWRD, npwrd);
        contentValues.put(KEY_DATE, date);
        if (bal.length()>0 && bal!=null) contentValues.put(KEY_BAL, bal);
        if (info.length()>0 && info!=null) contentValues.put(KEY_INFO, info);
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getData(String data,String column){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME+" where "+column+"="+data+"", null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }

//    public boolean updateContact (Integer id, String name, String phone, String email, String street,String place)
//    {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("name", name);
//        contentValues.put("phone", phone);
//        contentValues.put("email", email);
//        contentValues.put("street", street);
//        contentValues.put("place", place);
//        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
//        return true;
//    }

    public void delete(String column,String data){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_NAME+" where "+column+" = '"+data+"'");
        db.close();
    }

    public void delOldtrx(String column, String data){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+TABLE_NAME+" where "+KEY_ID+" < '"+data+"'");
        db.close();
    }

    public ArrayList<HashMap<String, String>> getAlltrx()
    {
        ArrayList<HashMap<String, String>> array_list = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME+" ORDER by ID DESC LIMIT 10000", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(KEY_ID,res.getString(res.getColumnIndex(KEY_ID)));
            map.put(KEY_NPWRD,res.getString(res.getColumnIndex(KEY_NPWRD)));
            map.put(KEY_NAME,res.getString(res.getColumnIndex(KEY_NAME)));
            map.put(KEY_AMOUNT,res.getString(res.getColumnIndex(KEY_AMOUNT)));
            map.put(KEY_BAL,res.getString(res.getColumnIndex(KEY_BAL)));
            map.put(KEY_REFF,res.getString(res.getColumnIndex(KEY_REFF)));
            map.put(KEY_DATE,res.getString(res.getColumnIndex(KEY_DATE)));
            map.put(KEY_INFO,res.getString(res.getColumnIndex(KEY_INFO)));
            array_list.add(map);
            res.moveToNext();
        }
        return array_list;
    }
    public ArrayList<HashMap<String, String>> getFiltertrx(String Filter)
    {
        ArrayList<HashMap<String, String>> array_list = new ArrayList<>();


        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res ;
        if (Filter!=null && Filter.length()>0) {
            res = db.rawQuery("select * from " + TABLE_NAME + " where " + KEY_NPWRD + " LIKE '%" + Filter + "%' OR " + KEY_NAME + " LIKE '%" + Filter + "%' ORDER by ID DESC LIMIT 10000", null);
        } else{
            res =  db.rawQuery( "select * from "+TABLE_NAME+" ORDER by ID DESC LIMIT 10000", null );
        }
        res.moveToFirst();

        while(res.isAfterLast() == false){
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(KEY_ID,res.getString(res.getColumnIndex(KEY_ID)));
            map.put(KEY_NPWRD,res.getString(res.getColumnIndex(KEY_NPWRD)));
            map.put(KEY_NAME,res.getString(res.getColumnIndex(KEY_NAME)));
            map.put(KEY_AMOUNT,res.getString(res.getColumnIndex(KEY_AMOUNT)));
            map.put(KEY_BAL,res.getString(res.getColumnIndex(KEY_BAL)));
            map.put(KEY_REFF,res.getString(res.getColumnIndex(KEY_REFF)));
            map.put(KEY_DATE,res.getString(res.getColumnIndex(KEY_DATE)));
            map.put(KEY_INFO,res.getString(res.getColumnIndex(KEY_INFO)));
            array_list.add(map);
            res.moveToNext();
        }
        return array_list;
    }
    public ArrayList<String> getAllmsg()
    {
        ArrayList<String> array_list = new ArrayList<String>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME2+" ORDER by ID DESC LIMIT 10000", null  );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndex(KEY_INFO))+"\n"+res.getString(res.getColumnIndex(KEY_DATE)));
            res.moveToNext();
        }
        return array_list;
    }
    public String getlastmsg()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME2+" ORDER by ID DESC LIMIT 1", null  );
        res.moveToFirst();
        if (res.getCount()>0){
            while(res.isAfterLast() == false){
                return (res.getString(res.getColumnIndex(KEY_INFO)));
            }
        }
        return "";
    }
//    public ArrayList<HashMap<String, String>> getAllmsg()
//    {
//        ArrayList<HashMap<String, String>> array_list = new ArrayList<>();
//
//        //hp = new HashMap();
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME2+" ORDER by ID DESC LIMIT 10000", null );
//        res.moveToFirst();
//
//        while(res.isAfterLast() == false){
//            HashMap<String, String> map = new HashMap<String, String>();
//            map.put(KEY_ID,res.getString(res.getColumnIndex(KEY_ID)));
//
//            map.put(KEY_DATE,res.getString(res.getColumnIndex(KEY_DATE)));
//            map.put(KEY_INFO,res.getString(res.getColumnIndex(KEY_INFO)));
//            array_list.add(map);
//            res.moveToNext();
//        }
//        return array_list;
//    }

}


