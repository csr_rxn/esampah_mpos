package com.bankjateng.eret;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragInq extends Fragment {
    public FragInq() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ActMain actMain =(ActMain) getActivity();
        View rootView = inflater.inflate(R.layout.fragment_item, container, false);
        actMain.EretPayData(rootView);
        return rootView;

    }

}
